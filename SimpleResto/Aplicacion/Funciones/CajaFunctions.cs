﻿using SimpleManager.Datos;
using SimpleManager.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleManager.Aplicacion.Funciones
{
    public static class CajaFunctions
    {

        public static decimal CalcularEfectivoEstimado (SimpleKioskContext db,int CajaId)
        {
            var ingresos = CalcularIngresos(db, CajaId);
            var egresos = CalcularEgresos(db,CajaId);
            var montoInicial = db.Cajas.Find(CajaId).MontoInicial;
            
            return  montoInicial - egresos + ingresos;
        }

        public static decimal CalcularIngresos(SimpleKioskContext db, int CajaId)
        {
            var caja = db.Cajas.Find(CajaId);

            var ingresos = db.Ventas
                    .Where(x => x.HoraInicio >= caja.FechaApertura)
                    .Where(x => x.HoraCierre <= caja.FechaCierre || !caja.FechaCierre.HasValue )
                    .Sum(x => x.Total);
            var movimientos = db.MovimientosCaja
                    .Where(x => x.CajaId == CajaId)
                    .Where(x => x.TipoMovimientoCajaId == (int)TiposMovimientoCaja.Deposito)
                    .Sum(x => x.Monto);

            return ingresos + movimientos;
        }
        
        public static decimal CalcularEgresos(SimpleKioskContext db, int CajaId)
        {
            var egresos = db.MovimientosCaja
                .Where(x => x.CajaId == CajaId)
                .Where(x => x.TipoMovimientoCajaId == (int)TiposMovimientoCaja.Retiro)
                .Sum(x => x.Monto);

            return egresos;
        }

        public static decimal CalcularTotal(SimpleKioskContext db, int CajaID)
        {
            var caja = db.Cajas.Find(CajaID);

            var ingresos = CalcularIngresos(db, CajaID);
            var montoInicial = caja.MontoInicial;
            var egresos = CalcularEgresos(db,CajaID);
            var efectivo = caja.EfectivoCaja;

            var result = efectivo - (montoInicial + ingresos - egresos);

            return result;
        }

    }
}
