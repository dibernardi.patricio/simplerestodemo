﻿using Microsoft.EntityFrameworkCore;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using System;
using System.Linq;

namespace SimpleManager.Aplicacion
{
    public static class Inicializador
    {
        public static void InicializarBasedeDatos()
        {
            var db = SimpleKioskContextFactory.CreateContext();

            if (!db.Database.EnsureCreated())
                db.Database.Migrate();

            if (db == null)
                throw new InvalidOperationException("No ambient DbContext of type UserManagementDbContext found. This means that this repository method has been called outside of the scope of a DbContextScope. A repository must only be accessed within the scope of a DbContextScope, which takes care of creating the DbContext instances that the repositories need and making them available as ambient contexts. This is what ensures that, for any given DbContext-derived type, the same instance is used throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory in your top-level business logic service method to create a DbContextScope that wraps the entire business transaction that your service method implements. Then access this repository within that scope. Refer to the comments in the IDbContextScope.cs file for more details.");

            if (!db.TiposMovimientoCaja.Any())
            {
                SeedTiposMovimientoCaja(db);
            }

            if (db.VentaTipos.Any())
            {
                return; //ya fue seedeada
            }


        }

        public static void InicializarMapeos()
        {
            AutoMapperConfiguration.Configure();
        }

        private static void SeedTiposMovimientoCaja (SimpleKioskContext db)
        {

            var entities = new[]
            {
                new TipoMovimientoCaja{ Descripcion = "Retiro"},
                new TipoMovimientoCaja{ Descripcion = "Deposito"}
            };

            db.TiposMovimientoCaja.AddRange(entities);
            db.SaveChanges();
        }

        private static void SeedVentaEstados(SimpleKioskContext db)
        {
            var entities = new[]
            {
                new OperacionEstado{Descripcion = "EnCurso"},
                new OperacionEstado{Descripcion = "Pagando"},
                new OperacionEstado{Descripcion = "Cerrada"},
                new OperacionEstado{Descripcion = "Anulada"}
            };

            db.OperacionEstados.AddRange(entities);
            db.SaveChanges();

        }

        private static void SeedVentaTipos(SimpleKioskContext db)
        {
            var entities = new[]
            {
                new VentaTipo{Descripcion = "Salon"},
                new VentaTipo{Descripcion = "Mostrador"}
            };

            db.VentaTipos.AddRange(entities);
            db.SaveChanges();

        }

    }
}

