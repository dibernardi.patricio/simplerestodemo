﻿using AutoMapper;
using SimpleManager.Aplicacion.Servicios.Ventas.Mapeadores;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => cfg.AddProfiles("SimpleManager.Aplicacion"));
        }
    }
}
