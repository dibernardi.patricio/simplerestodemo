﻿using FluentValidation;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;


namespace SimpleManager.Aplicacion.Servicios.MovimientosCaja.Comandos.CreateMovimientoCaja
{
    public class CreateMovimientoCajaValidator : AbstractValidator<MovimientoCaja>
    {
        public CreateMovimientoCajaValidator()
        {
            RuleFor(x => x.TipoMovimientoCajaId)
                .GreaterThan(0)
                .WithMessage("Por favor ingrese un Tipo de Movimiento");

            RuleFor(x => x.Monto)
                .NotEmpty()
                .WithMessage("Por favor ingrese un Monto");

            RuleFor(x => x.CajaId)
                .GreaterThan(0)
                .WithMessage("Por favor ingrese una Caja");
        }
    }
}
