﻿using FluentValidation.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.MovimientosCaja.Comandos.CreateMovimientoCaja
{
    public class CreateMovimientoCajaCommand : IRequest<ValidationResult>
    {
        public int CajaId { get; set; }
        public int TipoMovimientoCajaId { get; set; }
        public decimal Monto { get; set; }
        public string Comentario { get; set; }
    }
}
