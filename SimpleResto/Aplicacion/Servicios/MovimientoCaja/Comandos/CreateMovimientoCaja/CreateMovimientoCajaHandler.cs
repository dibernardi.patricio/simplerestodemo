﻿using AutoMapper;
using FluentValidation.Results;
using MediatR;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using SimpleManager.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.MovimientosCaja.Comandos.CreateMovimientoCaja
{
    public class CreateMovimientoCajaHandler : IRequestHandler<CreateMovimientoCajaCommand, ValidationResult>
    {
        private SimpleKioskContext db;

        public CreateMovimientoCajaHandler()
        {
            var dbContext = new SimpleKioskContext();//factory.createContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<ValidationResult> Handle(CreateMovimientoCajaCommand command, CancellationToken cancellationToken)
        {
            var MovimientoCaja = Mapper.Map<MovimientoCaja>(command);
            var validador = new CreateMovimientoCajaValidator();

            var message = validador.ValidateAsync(MovimientoCaja);
            await message;

            if (message.Result.IsValid)
            {
                MovimientoCaja.Fecha = DateTime.Now;
                db.MovimientosCaja.Add(MovimientoCaja);

                await db.SaveChangesAsync();
            }

            return message.Result;
        }
    }
}
