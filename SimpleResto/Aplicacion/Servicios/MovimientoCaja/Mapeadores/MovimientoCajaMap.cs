﻿using AutoMapper;
using SimpleManager.Aplicacion.Servicios.MovimientosCaja.Comandos.CreateMovimientoCaja;
using SimpleManager.Dominio.Entidades;

namespace SimpleManager.Aplicacion.Servicios.MovimientosCaja.Mapeadores
{
    class MovimientoCajaMap : Profile
    {
        public MovimientoCajaMap()
        {
            CreateMap<CreateMovimientoCajaCommand, MovimientoCaja>(MemberList.Source);
        //    CreateMap<VentaDetalle, MovimientoCaja>(MemberList.Source)
        //    .ForMember("MovimientoCajaId", opt => opt.Ignore())
        //    .ForMember(MovimientoCaja => MovimientoCaja.Diferencia, m => m.MapFrom(VentaDetalle => VentaDetalle.Cantidad))
        //    .BeforeMap((s, d) => d.Fecha = DateTime.Now);
        }
    }
}
