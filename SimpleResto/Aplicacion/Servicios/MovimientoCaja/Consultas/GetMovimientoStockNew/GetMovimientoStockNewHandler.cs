﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using System.Threading;
using SimpleManager.Datos;
using System.Threading.Tasks;
using System.Linq;
using SimpleManager.Aplicacion.Functions;
using Microsoft.EntityFrameworkCore;

namespace SimpleManager.Aplicacion.Servicios.MovimientosStock.Consultas.GetMovimientoStockNew
{
    public class GetMovimientoStockNewHandler : IRequestHandler<GetMovimientoStockNewQuery, MovimientoStockNewViewModel>
    {
        private Datos.SimpleKioskContext db;

        public GetMovimientoStockNewHandler()
        {
            var dbContext = new Datos.SimpleKioskContext();//factory.createContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<MovimientoStockNewViewModel> Handle(GetMovimientoStockNewQuery request, CancellationToken cancellationToken)
        {

            var eventosList = await db.Eventos.Where(x => x.Descripcion == "Ajuste Manual"
                                                     || x.Descripcion == "Ajuste por Desperdicio")
                                              .Select(x => new EventoLookUpModel
                                              {
                                                  EventoId = x.EventoId,
                                                  Descripcion = x.Descripcion
                                              }).ToListAsync();

            
            var productosList = db.Productos.Where(x => x.usaIngredientes == false).ToList();

            var resultList = new List<ProductoLookupModel>();

            productosList.ForEach(p =>
            {
                resultList.Add(new ProductoLookupModel
                {
                    ProductoId = p.ProductoId,
                    Nombre = p.Nombre,
                    StockActual = StockFunctions.CalcularStockProducto(db, p.ProductoId)
                });
            });

            var ingredientesList = await db.Ingredientes.Select(x => new IngredienteLookupModel
            {
                IngredienteId = x.IngredienteId,
                Nombre = x.Nombre
            }).ToListAsync();


            return new MovimientoStockNewViewModel
            {
                EventoLookupList = eventosList,
                ProductoLookupList = resultList,
                IngredienteLookupList = ingredientesList
            };
        }
    }
}
