﻿using System;
using System.Collections.Generic;
using System.Text;

using MediatR;


namespace SimpleManager.Aplicacion.Servicios.MovimientosStock.Consultas.GetMovimientoStockNew
{
    

    public class GetMovimientoStockNewQuery : IRequest<MovimientoStockNewViewModel>
    {
        public int MovimientoStockNewId { get; set; }
    }
}
