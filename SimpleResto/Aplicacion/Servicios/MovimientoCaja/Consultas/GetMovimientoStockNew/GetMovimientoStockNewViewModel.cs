﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.MovimientosStock.Consultas.GetMovimientoStockNew
{
    public class MovimientoStockNewViewModel
    {
        public IList<ProductoLookupModel> ProductoLookupList { get; set; }
        public IList<IngredienteLookupModel> IngredienteLookupList { get; set; }
        public IList<EventoLookUpModel> EventoLookupList { get; set; }

    }

    public class EventoLookUpModel
    {
        public int? EventoId { get; set; }
        public string Descripcion { get; set; }
    }
    public class ProductoLookupModel
    {
        public int ProductoId { get; set; }
        public string Nombre { get; set; }
        public decimal StockActual { get; set; }
    }
    public class IngredienteLookupModel
    {
        public int IngredienteId { get; set; }
        public string Nombre { get; set; }
        public decimal StockActual { get; set; }
    }
}
