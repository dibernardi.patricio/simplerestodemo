﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SimpleManager.Aplicacion.Servicios.MovimientosCaja.Consultas.GetMovimientosCaja;
using SimpleManager.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.MovimientosCaja.Consultas.GetMovimientosCaja
{
    public class GetMovimientosCajaHandler : IRequestHandler<GetMovimientosCajaQuery, MovimientoCajaViewModel>
    {
        private SimpleKioskContext db;

        public GetMovimientosCajaHandler()
        {
            var dbContext = new SimpleKioskContext();//factory.createContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<MovimientoCajaViewModel> Handle(GetMovimientosCajaQuery request, CancellationToken cancellationToken)
        {
            var movimientosList = await db.MovimientosCaja
                .Where (x => x.CajaId == request.CajaId) 
                .ToListAsync();

            //var eventosList = await db.Eventos.Select(x => new EventoLookUpModel
            //{
            //    EventoId =  (int?)x.EventoId,
            //    Descripcion = x.Descripcion
            //}).ToListAsync();
            //eventosList.Insert(0, new EventoLookUpModel {EventoId = null, Descripcion = "Sin Filtro" });

            return new MovimientoCajaViewModel { };
        }
    }
}

