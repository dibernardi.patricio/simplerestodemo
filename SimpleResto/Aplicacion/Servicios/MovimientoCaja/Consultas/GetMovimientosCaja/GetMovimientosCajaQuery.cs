﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.MovimientosCaja.Consultas.GetMovimientosCaja
{
    public class GetMovimientosCajaQuery : IRequest<MovimientoCajaViewModel>
    {
       public int CajaId { get; set; }
    }
}
