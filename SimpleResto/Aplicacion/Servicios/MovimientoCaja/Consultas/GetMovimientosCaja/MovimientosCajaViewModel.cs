﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.MovimientosCaja.Consultas.GetMovimientosCaja
{
    public class MovimientoCajaViewModel
    {
        public IList<rowMovimientoCaja> Movimientos { get; set; }
        //public IList EventosList { get; set; }
    }
    public class rowMovimientoCaja
    {
        public int MovimientoCajaId { get; set; }
        public string TipoMovimiento { get; set; }
        public decimal Monto { get; set; }
        //public DateTime Fecha { get; set; }
    }
    //public class EventoLookUpModel
    //{
    //    public int? EventoId { get; set; }
    //    public string Descripcion { get; set; }
    //}
}
