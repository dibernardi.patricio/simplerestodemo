﻿using FluentValidation;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CloseCaja
{
    public class CloseCajaValidator : AbstractValidator<Caja>
    {
        private SimpleKioskContext db;
        public CloseCajaValidator(SimpleKioskContext context)
        {
            db = context;

            RuleFor(x => x.CajaId)
                .NotEmpty()
                .WithMessage("Por favor Selecciones un Arqueo de Caja");

            RuleFor(x => x.EfectivoCaja)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Por favor ingrese el Efectivo en la Caja");

        }
    }
}
