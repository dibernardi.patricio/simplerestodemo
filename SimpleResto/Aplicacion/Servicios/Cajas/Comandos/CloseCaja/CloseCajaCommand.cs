﻿using FluentValidation.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CloseCaja
{
    public class CloseCajaCommand : IRequest<ValidationResult>
    {
        public int CajaId { get; set; }
        public decimal EfectivoCaja { get; set; }
        public string Comentario { get; set; }
    }
}
