﻿using AutoMapper;
using FluentValidation.Results;
using MediatR;
using SimpleManager.Aplicacion.Funciones;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using SimpleManager.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CloseCaja
{
    public class CloseCajaHandler : IRequestHandler<CloseCajaCommand, ValidationResult>
    {
        private SimpleKioskContext db;

        public CloseCajaHandler()
        {
            var dbContext = new SimpleKioskContext();//factory.CloseContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<ValidationResult> Handle(CloseCajaCommand command, CancellationToken cancellationToken)
        {
            var cajaCerrada = await db.Cajas.FindAsync(command.CajaId);
            cajaCerrada = Mapper.Map(command,cajaCerrada);
            
            var validador = new CloseCajaValidator(db);
            var message = validador.ValidateAsync(cajaCerrada);
            await message;

            if (message.Result.IsValid)
            {
                cajaCerrada.Total = CajaFunctions.CalcularTotal(db, cajaCerrada.CajaId);
                cajaCerrada.FechaCierre = DateTime.Now;
                cajaCerrada.OperacionEstadoId = (int)OperacionEstados.Cerrada;

                await db.SaveChangesAsync();
            }

            return message.Result;
        }
    }
}
