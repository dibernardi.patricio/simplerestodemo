﻿using FluentValidation.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CreateCaja
{
    public class CreateCajaCommand : IRequest<ValidationResult>
    {
        public DateTime FechaApertura { get; set; }
        public Decimal MontoInicial { get; set; }

    }
}
