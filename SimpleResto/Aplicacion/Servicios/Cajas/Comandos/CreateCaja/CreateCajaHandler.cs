﻿using AutoMapper;
using FluentValidation.Results;
using MediatR;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using SimpleManager.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CreateCaja
{
    public class CreateCajaHandler : IRequestHandler<CreateCajaCommand, ValidationResult>
    {
        private SimpleKioskContext db;

        public CreateCajaHandler()
        {
            var dbContext = new SimpleKioskContext();//factory.createContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<ValidationResult> Handle(CreateCajaCommand command, CancellationToken cancellationToken)
        {
            var NuevaCaja = Mapper.Map<Caja>(command);
            var validador = new CreateCajaValidator(db);

            var message = validador.ValidateAsync(NuevaCaja);
            await message;

            if (message.Result.IsValid)
            {
                NuevaCaja.OperacionEstadoId = (int)OperacionEstados.EnCurso;
                db.Cajas.Add(NuevaCaja);

                await db.SaveChangesAsync();
            }

            return message.Result;
        }
    }
}
