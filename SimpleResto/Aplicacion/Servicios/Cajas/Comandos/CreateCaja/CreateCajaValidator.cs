﻿using FluentValidation;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using SimpleManager.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CreateCaja
{
    public class CreateCajaValidator : AbstractValidator<Caja>
    {
        private SimpleKioskContext db;
        public CreateCajaValidator(SimpleKioskContext context)
        {
            db = context;

            RuleFor(x => x.FechaApertura)
                .NotEmpty()
                .WithMessage("Por favor ingrese un Fecha Valida");

            RuleFor(x => x.MontoInicial)
                .GreaterThanOrEqualTo(0)
                .WithMessage("Por favor ingrese un Monto Inicial");

            RuleFor(x => x)
                .Must((request, val,ret) =>
                {
                    var cajaAbierta = db.Cajas
                            .Where(x => x.OperacionEstadoId == (int)OperacionEstados.EnCurso).ToList();

                    if (cajaAbierta.Count > 0)
                    {
                        return false;
                    }
                    return true;
                })
                .WithMessage("Existe una caja Abierta");


        }
    }
}
