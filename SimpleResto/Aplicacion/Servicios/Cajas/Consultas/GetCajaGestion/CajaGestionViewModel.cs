﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaGestion
{
    public class CajaGestionViewModel
    {
        public IList<rowCaja> gridCajas { get; set; }
    }
	
	public class rowCaja
    {
        public int CajaId { get; set; }
        public DateTime FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }

        public decimal MontoInicial { get; set; }
        public decimal EfectivoCaja { get; set; }

        public string Total { get; set; }
    }
	
}
