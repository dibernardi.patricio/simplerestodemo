﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaGestion
{
    public class GetCajaGestion : IRequestHandler<GetCajaGestionQuery, CajaGestionViewModel>
    {
        private SimpleKioskContext db;

        public GetCajaGestion()
        {
            var dbContext = new SimpleKioskContext();//factory.createContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<CajaGestionViewModel> Handle(GetCajaGestionQuery request, CancellationToken cancellationToken)
        {
            var entityList = await db.Cajas
                //.Where(x => x.FechaApertura >= DateTime.Now.AddHours(-36))
                .OrderByDescending(x => x.FechaApertura)
                .ToListAsync();

            var CajasList = Mapper.Map<List<Caja>, List<rowCaja>>(entityList);

            return new CajaGestionViewModel { gridCajas = CajasList ?? null };
        }
    }
}
