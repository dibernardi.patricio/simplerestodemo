﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDetalle
{
    public class GetCajaDetalleQuery : IRequest<CajaDetalleViewModel>
    {
        public int CajaId { get; set; }
    }
}
