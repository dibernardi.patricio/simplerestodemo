﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDetalle
{
    public class CajaDetalleViewModel
    {
        public int CajaId { get; set; }
        public DateTime FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }

        public decimal MontoInicial { get; set; }
        public decimal Ingresos { get; set; }
        public decimal Egresos { get; set; }
        public decimal EfectivoEstimado { get; set; }
        public decimal EfectivoCaja { get; set; }
        public decimal Total { get; set; }
        public string Comentario { get; set; }

        public bool PermiteEditar { get; set; }

        public IList<rowMovimientoCaja> gridMovimientosCaja { get; set; }

        public class rowMovimientoCaja
        {
            public string Tipo { get; set; }
            public decimal Monto { get; set; }
            public DateTime Fecha { get; set; }
        }

    }
}
