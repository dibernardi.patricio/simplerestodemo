﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SimpleManager.Aplicacion.Exceptions;
using SimpleManager.Aplicacion.Funciones;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using SimpleManager.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDetalle.CajaDetalleViewModel;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDetalle
{
    public class GetCajaDetalleHandler : IRequestHandler<GetCajaDetalleQuery, CajaDetalleViewModel>
    {
        private SimpleKioskContext db;

        public GetCajaDetalleHandler()
        {
            var dbContext = new SimpleKioskContext();//factory.createContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<CajaDetalleViewModel> Handle(GetCajaDetalleQuery request, CancellationToken cancellationToken)
        {
            var result = new CajaDetalleViewModel();

       
                var consulta = await db.Cajas.FindAsync(request.CajaId);
                if (consulta == null)
                    throw new NoSeEncontroEntidad(nameof(Caja), request.CajaId);

                result = Mapper.Map<Caja, CajaDetalleViewModel>(consulta);
                result.PermiteEditar = (consulta.OperacionEstadoId != (int)OperacionEstados.Cerrada);

            if (request.CajaId > 0)
            {
                //Movimientos
                var gridMovimientos = await db.MovimientosCaja
                .Include("TipoMovimientoCaja")
                .Where (x => x.CajaId == request.CajaId)
                .Select(x => new rowMovimientoCaja
                {
                    Tipo = x.TipoMovimientoCaja.Descripcion,
                    Monto = x.Monto,
                    Fecha = x.Fecha
                }).ToListAsync();
                result.gridMovimientosCaja = gridMovimientos;

                //Calculos
                result.Ingresos = CajaFunctions.CalcularIngresos(db, request.CajaId);
                result.Egresos = CajaFunctions.CalcularEgresos(db, request.CajaId);
                result.EfectivoEstimado = CajaFunctions.CalcularEfectivoEstimado(db, request.CajaId);
                if (result.PermiteEditar)
                {
                    result.Total = CajaFunctions.CalcularTotal(db, request.CajaId);
                }
            }

            return result ;
        }
    }
}
