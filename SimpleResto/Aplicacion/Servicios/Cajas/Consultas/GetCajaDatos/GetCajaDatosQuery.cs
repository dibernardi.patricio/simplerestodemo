﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDatos
{
    public class GetCajaDatosQuery : IRequest<GetCajaDatosViewModel>
    {
        public int CajaId { get; set; }
    }
}
