﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDatos
{
    public class GetCajaDatosViewModel
    {
        public int CajaId { get; set; }
        public DateTime FechaApertura { get; set; }
        public decimal MontoInicial { get; set; }
}
}
