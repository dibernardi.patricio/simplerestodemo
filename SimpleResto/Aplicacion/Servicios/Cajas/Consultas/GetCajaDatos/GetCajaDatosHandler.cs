﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SimpleManager.Datos;
using SimpleManager.Dominio.Entidades;
using SimpleManager.Dominio.Enums;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDatos
{
    public class GetCajaDatos : IRequestHandler<GetCajaDatosQuery, GetCajaDatosViewModel>
    {
        private SimpleKioskContext db;

        public GetCajaDatos()
        {
            var dbContext = new SimpleKioskContext();//factory.createContext;
            db = dbContext ?? throw new InvalidOperationException("No DbContext !");
        }

        public async Task<GetCajaDatosViewModel> Handle(GetCajaDatosQuery request, CancellationToken cancellationToken)
        {

            Caja ultimaCaja;
            DateTime? fechaPrimeraVenta;
            DateTime fechaUltimoCierre;

            if (request.CajaId > 0)
            {
                ultimaCaja = await db.Cajas.FindAsync(request.CajaId);
            }
            else
            {
                //calculo fecha apertura
                 ultimaCaja = await db.Cajas
                                        .Where (x => x.OperacionEstadoId == (int)OperacionEstados.Cerrada)
                                        .OrderByDescending(x => x.CajaId)
                                        .FirstOrDefaultAsync();

                if (ultimaCaja == null)
                {
                    fechaPrimeraVenta = await db.Ventas.MinAsync(x => (DateTime?)x.HoraInicio);
                    fechaUltimoCierre = fechaPrimeraVenta ?? DateTime.Now;
                    ultimaCaja = new Caja { FechaCierre = fechaUltimoCierre };
                }
            }

            var result = new GetCajaDatosViewModel { FechaApertura = (DateTime)ultimaCaja.FechaCierre };

            return result;

        }
    }
}
