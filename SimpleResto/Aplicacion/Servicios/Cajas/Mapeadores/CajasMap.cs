﻿using AutoMapper;
using SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CloseCaja;
using SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CreateCaja;
using SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDatos;
using SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDetalle;
using SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaGestion;
using SimpleManager.Dominio.Entidades;

namespace SimpleManager.Aplicacion.Servicios.Ventas.Mapeadores
{
    class CajaMap : Profile
    {
        public CajaMap()
        {
            CreateMap<CreateCajaCommand, Caja>(MemberList.Source);
            CreateMap<CloseCajaCommand, Caja>(MemberList.Source);
            //CreateMap<UpdateCajaCommand, Caja>(MemberList.Source);
            CreateMap<Caja, rowCaja>(MemberList.Source);
                

            CreateMap<Caja, GetCajaDatosViewModel>(MemberList.Source);
            CreateMap<Caja, CajaDetalleViewModel>(MemberList.Source);

            //CreateMap<CreateCajaDetalleCommand, CajaDetalle>(MemberList.Source);


            //CreateMap<CajaDetalle, MovimientoStock>(MemberList.Source)
            //  .ForMember("MovimientoStockId", opt => opt.Ignore())
            //  .BeforeMap((s, d) => d.Fecha = DateTime.Now);

            //CreateMap<CreateVentaDetalleCommand, VentaDetalle>(MemberList.Source);
            //CreateMap<VentaDetalle, MovimientoStock>(MemberList.Source)
            //.ForMember("MovimientoStockId", opt => opt.Ignore())
            //.ForMember(MovimientoStock => MovimientoStock.Diferencia, m => m.MapFrom(VentaDetalle => VentaDetalle.Cantidad))
            //.BeforeMap((s, d) => d.Fecha = DateTime.Now);
        }
    }
}
