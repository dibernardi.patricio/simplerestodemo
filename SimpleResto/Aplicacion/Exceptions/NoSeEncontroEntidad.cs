﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Aplicacion.Exceptions
{
    public class NoSeEncontroEntidad:Exception
    {
            public NoSeEncontroEntidad(string name, object key)
                : base($"Entidad \"{name}\" ({key}) no se encontro.")
            {
            }
        
    }
}
