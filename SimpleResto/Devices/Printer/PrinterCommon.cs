﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SimpleManager.Devices.Printer
{
    public class PrinterCommons
    {
        public static IList  GetAllPrinters()
        {
            var ImpresorasList = new List<string>();

            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                ImpresorasList.Add(printer);
            }
            return ImpresorasList;
        }

        public static string GetReceiptPrinter()
        {
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                if (printer.ToLower().Contains("receipt"))
                {
                    return printer;
                }
            }
            return null;
        }


        public static void Print(StringBuilder ticket,string impresora)
        {
            RawPrinterHelper.SendStringToPrinter(impresora, ticket.ToString()); //Imprime texto.
            //RawPrinterHelper.ConvertStringToBytes(ticket.ToString(), impresora);

        }
    }
}

