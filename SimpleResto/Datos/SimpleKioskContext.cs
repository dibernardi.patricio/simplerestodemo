﻿using Microsoft.EntityFrameworkCore;
using SimpleManager.Dominio.Entidades;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SimpleManager.Datos
{
    public class SimpleKioskContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //string executable = Assembly.GetExecutingAssembly().Location;
            //string path = (System.IO.Path.GetDirectoryName(executable));

            string workingDirectory = Environment.CurrentDirectory;
            string projectDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName;

            optionsBuilder.UseSqlServer(string.Format(@"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename={0}\Datos\LocalDB\SimpleKiosDB.mdf;Integrated Security = True", projectDirectory));
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
       

        public DbSet<Venta> Ventas { get; set; }

        public DbSet<Caja> Cajas { get; set; }

        public DbSet<OperacionEstado> OperacionEstados { get; set; }
        public DbSet<VentaTipo> VentaTipos { get; set; }
        public DbSet<MovimientoCaja> MovimientosCaja { get; set; }

        public DbSet<TipoMovimientoCaja> TiposMovimientoCaja { get; set; }
    }
}
