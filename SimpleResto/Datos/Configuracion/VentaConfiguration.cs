﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleManager.Dominio.Entidades;

namespace SimpleManager.Datos.Configuracion
{
    public class VentaConfiguration : IEntityTypeConfiguration<Venta>
    {
        public void Configure(EntityTypeBuilder<Venta> builder)
        {
            //Entity
            builder.HasKey(x => x.VentaId);
            
            //.HasOne(e => e.Company)
            //.WithMany(c => c.Employees);


            //Property Configurations
            //modelBuilder.Entity<Student>()
            //        .Property(s => s.StudentId)
            //        .HasColumnName("Id")
            //        .HasDefaultValue(0)
            //        .IsRequired();
            //builder.Property(x => x.PersonasCantidad);

            //modelBuilder.Entity<Student>()
            //.HasOne<Grade>(s => s.Grade)
            //.WithMany(g => g.Students)
            //.HasForeignKey(s => s.CurrentGradeId);

        }
    }
}
