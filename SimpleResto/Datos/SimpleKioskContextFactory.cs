﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Datos
{
    public class SimpleKioskContextFactory
    {
        public static SimpleKioskContext CreateContext()
        {
            return new SimpleKioskContext();
        }

    }
}
