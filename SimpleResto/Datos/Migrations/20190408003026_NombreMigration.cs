﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SimpleManager.Datos.Migrations
{
    public partial class NombreMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MovimientoCajaTipoId",
                table: "MovimientosCaja",
                newName: "TipoMovimientoCajaId");

            migrationBuilder.CreateTable(
                name: "TiposMovimientoCaja",
                columns: table => new
                {
                    TipoMovimientoCajaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposMovimientoCaja", x => x.TipoMovimientoCajaId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovimientosCaja_TipoMovimientoCajaId",
                table: "MovimientosCaja",
                column: "TipoMovimientoCajaId");

            migrationBuilder.AddForeignKey(
                name: "FK_MovimientosCaja_TiposMovimientoCaja_TipoMovimientoCajaId",
                table: "MovimientosCaja",
                column: "TipoMovimientoCajaId",
                principalTable: "TiposMovimientoCaja",
                principalColumn: "TipoMovimientoCajaId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovimientosCaja_TiposMovimientoCaja_TipoMovimientoCajaId",
                table: "MovimientosCaja");

            migrationBuilder.DropTable(
                name: "TiposMovimientoCaja");

            migrationBuilder.DropIndex(
                name: "IX_MovimientosCaja_TipoMovimientoCajaId",
                table: "MovimientosCaja");

            migrationBuilder.RenameColumn(
                name: "TipoMovimientoCajaId",
                table: "MovimientosCaja",
                newName: "MovimientoCajaTipoId");
        }
    }
}
