﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SimpleManager.Dominio.Enums
{
        public enum OperacionEstados
        {
            EnCurso = 1,
            Pagando = 2,
            Cerrada = 3,
            Anulada = 4
        }
   
}
