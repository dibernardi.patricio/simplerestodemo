﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Dominio.Enums
{
    public enum TiposMovimientoCaja
    {
        Retiro = 1,
        Deposito = 2
    }

}
