﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Dominio.Entidades
{
    public class VentaTipo
    {
        public int VentaTipoId { get; set; }
        public string Descripcion { get; set; }

    }
}
