﻿using SimpleManager.Dominio.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Dominio.Entidades
{
    public class OperacionEstado
    {
        public int OperacionEstadoId { get; set; }
        public string Descripcion { get; set; }

        public ICollection<Venta> Ventas { get; set; }
    }
}
