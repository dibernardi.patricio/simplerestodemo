﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Dominio.Entidades
{
    public class MovimientoCaja
    {
        public int MovimientoCajaId { get; set; }
        public int CajaId { get; set; }
        public int TipoMovimientoCajaId { get; set; }
        public decimal Monto { get; set; }
        public string Comentario { get; set; }

        public DateTime Fecha { get; set; }

        public TipoMovimientoCaja TipoMovimientoCaja { get; set; }

    }
}
