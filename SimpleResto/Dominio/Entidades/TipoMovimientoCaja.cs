﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Dominio.Entidades
{
    public class TipoMovimientoCaja
    {
       public int TipoMovimientoCajaId { get; set; }
       public string Descripcion { get; set; }
    }
}
