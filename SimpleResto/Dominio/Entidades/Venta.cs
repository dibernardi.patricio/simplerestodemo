﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Dominio.Entidades
{
    public class Venta
    {
        public Venta()
        {
        }

        public int VentaId { get; set; }
        public string ClienteNombre { get; set; }
        public int PersonasCantidad { get; set; }

        public int? MesaId { get; set; }
        public int? MozoId { get; set; }
        public string Observacion { get; set; }
        public int VentaTipoId { get; set; }
        public int OperacionEstadoId { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime? HoraCierre { get; set; }
        public decimal DescuentoPorcentaje { get; set; }
        public decimal Total { get; set; }

        public VentaTipo VentaTipo { get; set; }
        public OperacionEstado OperacionEstado { get; set; }

    }
}
