﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleManager.Dominio.Entidades
{
    public class Caja
    {
        public int CajaId { get; set; }

        public DateTime FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }

        public decimal MontoInicial { get; set; }
        public decimal EfectivoCaja { get; set; }
        public decimal Total { get; set; }

        public int OperacionEstadoId { get; set; }

        public string Comentario { get; set; }
        
        //Relaciones

    }
}
