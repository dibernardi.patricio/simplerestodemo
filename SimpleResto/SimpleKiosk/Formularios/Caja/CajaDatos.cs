﻿using DevExpress.XtraEditors;
using MediatR;
using SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CreateCaja;
using SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDatos;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleManager.SimpleKiosk.Formularios.Caja
{
    public partial class CajaDatos : DevExpress.XtraEditors.XtraForm
    {
        private int CajaId;
        private IMediator Mediator;

        public CajaDatos(IMediator mediator,int id = 0)
        {
            InitializeComponent();
            Mediator = mediator;

            this.CajaId = id;
        }

        private async Task PresentarDatos()
        {
            var task = Mediator.Send(new GetCajaDatosQuery { CajaId = CajaId });
            await task;

            var vmCajaDatos = task.Result;

            dtFechaApertura.EditValue = vmCajaDatos.FechaApertura;
            txtMontoInicial.EditValue = vmCajaDatos.MontoInicial;
        }
            
        private void botonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void botonCrear_Click(object sender, EventArgs e)
        {
            //if (CajaId > 0)
            //{
            //    var message = Mediator.Send(new UpdateCajaCommand
            //    {
            //        CajaId = CajaId,
            //        Fecha = (DateTime)dtFecha.EditValue,
            //        ProveedorId = (int?)searchProveedor.EditValue,
            //        ComprobanteNumero = txtComprobanteNumero.ToString(),
            //        Observacion = txtObservacion.Text
            //    });
            //    await message;
            //}
            //else
            //{

            var message = Mediator.Send(new CreateCajaCommand
            {
                FechaApertura = (DateTime)dtFechaApertura.EditValue,
                MontoInicial = (decimal)txtMontoInicial.EditValue
            });
            await message;
            if (message.Result.IsValid)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                XtraMessageBox.Show(message.Result.Errors.FirstOrDefault().ErrorMessage);
                return;
            }
            //}

        }

        private async void PedidoDatos_Load(object sender, EventArgs e)
        {
            await PresentarDatos();
        }
    }
}
