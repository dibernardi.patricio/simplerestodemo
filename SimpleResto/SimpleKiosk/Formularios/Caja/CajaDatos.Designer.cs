﻿namespace SimpleManager.SimpleKiosk.Formularios.Caja
{
    partial class CajaDatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CajaDatos));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.botonCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.botonCrear = new DevExpress.XtraEditors.SimpleButton();
            this.txtMontoInicial = new DevExpress.XtraEditors.TextEdit();
            this.dtFechaApertura = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoInicial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaApertura.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaApertura.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.botonCancelar);
            this.layoutControl1.Controls.Add(this.botonCrear);
            this.layoutControl1.Controls.Add(this.txtMontoInicial);
            this.layoutControl1.Controls.Add(this.dtFechaApertura);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(413, 172);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // botonCancelar
            // 
            this.botonCancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.botonCancelar.Appearance.Options.UseFont = true;
            this.botonCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botonCancelar.ImageOptions.Image")));
            this.botonCancelar.Location = new System.Drawing.Point(209, 108);
            this.botonCancelar.Name = "botonCancelar";
            this.botonCancelar.Size = new System.Drawing.Size(180, 38);
            this.botonCancelar.StyleController = this.layoutControl1;
            this.botonCancelar.TabIndex = 11;
            this.botonCancelar.Text = "Cancelar";
            this.botonCancelar.Click += new System.EventHandler(this.botonCancelar_Click);
            // 
            // botonCrear
            // 
            this.botonCrear.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.botonCrear.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.botonCrear.Appearance.Options.UseBackColor = true;
            this.botonCrear.Appearance.Options.UseFont = true;
            this.botonCrear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botonCrear.ImageOptions.Image")));
            this.botonCrear.Location = new System.Drawing.Point(24, 108);
            this.botonCrear.Name = "botonCrear";
            this.botonCrear.Size = new System.Drawing.Size(181, 38);
            this.botonCrear.StyleController = this.layoutControl1;
            this.botonCrear.TabIndex = 12;
            this.botonCrear.Text = "Guardar";
            this.botonCrear.Click += new System.EventHandler(this.botonCrear_Click);
            // 
            // txtMontoInicial
            // 
            this.txtMontoInicial.Location = new System.Drawing.Point(146, 66);
            this.txtMontoInicial.Name = "txtMontoInicial";
            this.txtMontoInicial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtMontoInicial.Properties.Appearance.Options.UseFont = true;
            this.txtMontoInicial.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMontoInicial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMontoInicial.Properties.Mask.EditMask = "c2";
            this.txtMontoInicial.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMontoInicial.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtMontoInicial.Size = new System.Drawing.Size(231, 26);
            this.txtMontoInicial.StyleController = this.layoutControl1;
            this.txtMontoInicial.TabIndex = 5;
            // 
            // dtFechaApertura
            // 
            this.dtFechaApertura.EditValue = null;
            this.dtFechaApertura.Location = new System.Drawing.Point(146, 36);
            this.dtFechaApertura.Name = "dtFechaApertura";
            this.dtFechaApertura.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.dtFechaApertura.Properties.Appearance.Options.UseFont = true;
            this.dtFechaApertura.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFechaApertura.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtFechaApertura.Properties.CalendarTimeProperties.Mask.EditMask = "G";
            this.dtFechaApertura.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista;
            this.dtFechaApertura.Properties.DisplayFormat.FormatString = "G";
            this.dtFechaApertura.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtFechaApertura.Properties.EditFormat.FormatString = "G";
            this.dtFechaApertura.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtFechaApertura.Properties.Mask.EditMask = "G";
            this.dtFechaApertura.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtFechaApertura.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            this.dtFechaApertura.Size = new System.Drawing.Size(231, 26);
            this.dtFechaApertura.StyleController = this.layoutControl1;
            this.dtFechaApertura.TabIndex = 14;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(413, 172);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Datos Pedido";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup2,
            this.layoutControlItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(393, 152);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.botonCancelar;
            this.layoutControlItem1.Location = new System.Drawing.Point(185, 84);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(184, 44);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(369, 84);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtMontoInicial;
            this.layoutControlItem2.CustomizationFormText = "Comprobante";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F);
            this.layoutControlItem2.OptionsPrint.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Size = new System.Drawing.Size(345, 30);
            this.layoutControlItem2.Text = "Monto Inicial";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(107, 19);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.dtFechaApertura;
            this.layoutControlItem6.CustomizationFormText = "Fecha";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(345, 30);
            this.layoutControlItem6.Text = "Fecha Apertura";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(107, 19);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.botonCrear;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(185, 44);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // CajaDatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 172);
            this.Controls.Add(this.layoutControl1);
            this.Name = "CajaDatos";
            this.Text = "Datos de la Caja";
            this.Load += new System.EventHandler(this.PedidoDatos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoInicial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaApertura.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFechaApertura.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.SimpleButton botonCancelar;
        private DevExpress.XtraEditors.SimpleButton botonCrear;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txtMontoInicial;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DateEdit dtFechaApertura;
    }
}