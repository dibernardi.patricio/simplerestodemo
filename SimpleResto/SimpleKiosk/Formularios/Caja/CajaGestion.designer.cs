﻿namespace SimpleManager.SimpleKiosk.Formularios.Caja
{
    partial class CajaGestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CajaGestion));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridMovimientosCaja = new DevExpress.XtraGrid.GridControl();
            this.gridViewMovimientosCaja = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.botonNuevoMovimiento = new DevExpress.XtraEditors.SimpleButton();
            this.lblTotal = new DevExpress.XtraEditors.LabelControl();
            this.lblCierreParcial = new DevExpress.XtraEditors.LabelControl();
            this.botonCerrarCaja = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtEfectivoCaja = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblEgresos = new DevExpress.XtraEditors.LabelControl();
            this.lblMontoInicial = new DevExpress.XtraEditors.LabelControl();
            this.lblIngresos = new DevExpress.XtraEditors.LabelControl();
            this.lblHoraApertura = new DevExpress.XtraEditors.LabelControl();
            this.lblHoraCierre = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridCajas = new DevExpress.XtraGrid.GridControl();
            this.gridViewCajas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.botonNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtComentario = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.frameCaja = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMovimientosCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMovimientosCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEfectivoCaja.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCajas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCajas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComentario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridMovimientosCaja);
            this.layoutControl1.Controls.Add(this.botonNuevoMovimiento);
            this.layoutControl1.Controls.Add(this.lblTotal);
            this.layoutControl1.Controls.Add(this.lblCierreParcial);
            this.layoutControl1.Controls.Add(this.botonCerrarCaja);
            this.layoutControl1.Controls.Add(this.labelControl16);
            this.layoutControl1.Controls.Add(this.labelControl15);
            this.layoutControl1.Controls.Add(this.txtEfectivoCaja);
            this.layoutControl1.Controls.Add(this.labelControl6);
            this.layoutControl1.Controls.Add(this.lblEgresos);
            this.layoutControl1.Controls.Add(this.lblMontoInicial);
            this.layoutControl1.Controls.Add(this.lblIngresos);
            this.layoutControl1.Controls.Add(this.lblHoraApertura);
            this.layoutControl1.Controls.Add(this.lblHoraCierre);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.gridCajas);
            this.layoutControl1.Controls.Add(this.botonNuevo);
            this.layoutControl1.Controls.Add(this.labelControl5);
            this.layoutControl1.Controls.Add(this.labelControl7);
            this.layoutControl1.Controls.Add(this.labelControl9);
            this.layoutControl1.Controls.Add(this.txtComentario);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1400, 822);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridMovimientosCaja
            // 
            this.gridMovimientosCaja.Location = new System.Drawing.Point(942, 238);
            this.gridMovimientosCaja.MainView = this.gridViewMovimientosCaja;
            this.gridMovimientosCaja.Name = "gridMovimientosCaja";
            this.gridMovimientosCaja.Size = new System.Drawing.Size(422, 127);
            this.gridMovimientosCaja.TabIndex = 33;
            this.gridMovimientosCaja.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMovimientosCaja});
            // 
            // gridViewMovimientosCaja
            // 
            this.gridViewMovimientosCaja.GridControl = this.gridMovimientosCaja;
            this.gridViewMovimientosCaja.Name = "gridViewMovimientosCaja";
            this.gridViewMovimientosCaja.OptionsView.ShowGroupPanel = false;
            // 
            // botonNuevoMovimiento
            // 
            this.botonNuevoMovimiento.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("botonNuevoMovimiento.Appearance.Image")));
            this.botonNuevoMovimiento.Appearance.Options.UseImage = true;
            this.botonNuevoMovimiento.Enabled = false;
            this.botonNuevoMovimiento.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botonNuevoMovimiento.ImageOptions.Image")));
            this.botonNuevoMovimiento.Location = new System.Drawing.Point(942, 198);
            this.botonNuevoMovimiento.Name = "botonNuevoMovimiento";
            this.botonNuevoMovimiento.Size = new System.Drawing.Size(422, 36);
            this.botonNuevoMovimiento.StyleController = this.layoutControl1;
            this.botonNuevoMovimiento.TabIndex = 32;
            this.botonNuevoMovimiento.Text = "Nuevo Movimiento ";
            this.botonNuevoMovimiento.Click += new System.EventHandler(this.botonNuevoMovimiento_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.Appearance.BackColor = System.Drawing.Color.DimGray;
            this.lblTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Appearance.Options.UseBackColor = true;
            this.lblTotal.Appearance.Options.UseFont = true;
            this.lblTotal.Appearance.Options.UseTextOptions = true;
            this.lblTotal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblTotal.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.lblTotal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTotal.Location = new System.Drawing.Point(1090, 631);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.lblTotal.MinimumSize = new System.Drawing.Size(0, 25);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(274, 25);
            this.lblTotal.StyleController = this.layoutControl1;
            this.lblTotal.TabIndex = 31;
            this.lblTotal.Text = "$0,00";
            // 
            // lblCierreParcial
            // 
            this.lblCierreParcial.Appearance.BackColor = System.Drawing.Color.DimGray;
            this.lblCierreParcial.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCierreParcial.Appearance.Options.UseBackColor = true;
            this.lblCierreParcial.Appearance.Options.UseFont = true;
            this.lblCierreParcial.Appearance.Options.UseTextOptions = true;
            this.lblCierreParcial.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblCierreParcial.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.lblCierreParcial.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCierreParcial.Location = new System.Drawing.Point(1088, 518);
            this.lblCierreParcial.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.lblCierreParcial.MinimumSize = new System.Drawing.Size(0, 25);
            this.lblCierreParcial.Name = "lblCierreParcial";
            this.lblCierreParcial.Size = new System.Drawing.Size(279, 25);
            this.lblCierreParcial.StyleController = this.layoutControl1;
            this.lblCierreParcial.TabIndex = 30;
            this.lblCierreParcial.Text = "$0,00";
            // 
            // botonCerrarCaja
            // 
            this.botonCerrarCaja.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("botonCerrarCaja.Appearance.Image")));
            this.botonCerrarCaja.Appearance.Options.UseImage = true;
            this.botonCerrarCaja.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botonCerrarCaja.ImageOptions.Image")));
            this.botonCerrarCaja.Location = new System.Drawing.Point(942, 750);
            this.botonCerrarCaja.Name = "botonCerrarCaja";
            this.botonCerrarCaja.Size = new System.Drawing.Size(422, 36);
            this.botonCerrarCaja.StyleController = this.layoutControl1;
            this.botonCerrarCaja.TabIndex = 29;
            this.botonCerrarCaja.Text = "Cerrar Caja";
            this.botonCerrarCaja.Click += new System.EventHandler(this.botonCerrarCaja_Click);
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Location = new System.Drawing.Point(942, 660);
            this.labelControl16.MinimumSize = new System.Drawing.Size(125, 25);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(125, 25);
            this.labelControl16.StyleController = this.layoutControl1;
            this.labelControl16.TabIndex = 27;
            this.labelControl16.Text = "Comentario:";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(942, 105);
            this.labelControl15.MaximumSize = new System.Drawing.Size(125, 0);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(125, 19);
            this.labelControl15.StyleController = this.layoutControl1;
            this.labelControl15.TabIndex = 26;
            this.labelControl15.Text = "Hora Cierre:";
            // 
            // txtEfectivoCaja
            // 
            this.txtEfectivoCaja.EditValue = "$0,00";
            this.txtEfectivoCaja.Location = new System.Drawing.Point(1229, 570);
            this.txtEfectivoCaja.MaximumSize = new System.Drawing.Size(135, 0);
            this.txtEfectivoCaja.Name = "txtEfectivoCaja";
            this.txtEfectivoCaja.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEfectivoCaja.Properties.Appearance.Options.UseFont = true;
            this.txtEfectivoCaja.Properties.Appearance.Options.UseTextOptions = true;
            this.txtEfectivoCaja.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtEfectivoCaja.Size = new System.Drawing.Size(135, 32);
            this.txtEfectivoCaja.StyleController = this.layoutControl1;
            this.txtEfectivoCaja.TabIndex = 23;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(942, 570);
            this.labelControl6.MinimumSize = new System.Drawing.Size(125, 25);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(138, 25);
            this.labelControl6.StyleController = this.layoutControl1;
            this.labelControl6.TabIndex = 17;
            this.labelControl6.Text = "Efectivo en Caja:";
            // 
            // lblEgresos
            // 
            this.lblEgresos.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEgresos.Appearance.Options.UseFont = true;
            this.lblEgresos.Location = new System.Drawing.Point(1314, 491);
            this.lblEgresos.MinimumSize = new System.Drawing.Size(0, 25);
            this.lblEgresos.Name = "lblEgresos";
            this.lblEgresos.Size = new System.Drawing.Size(50, 25);
            this.lblEgresos.StyleController = this.layoutControl1;
            this.lblEgresos.TabIndex = 16;
            this.lblEgresos.Text = "$0,00";
            // 
            // lblMontoInicial
            // 
            this.lblMontoInicial.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontoInicial.Appearance.Options.UseFont = true;
            this.lblMontoInicial.Appearance.Options.UseTextOptions = true;
            this.lblMontoInicial.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblMontoInicial.Location = new System.Drawing.Point(1314, 433);
            this.lblMontoInicial.MinimumSize = new System.Drawing.Size(0, 25);
            this.lblMontoInicial.Name = "lblMontoInicial";
            this.lblMontoInicial.Size = new System.Drawing.Size(50, 25);
            this.lblMontoInicial.StyleController = this.layoutControl1;
            this.lblMontoInicial.TabIndex = 14;
            this.lblMontoInicial.Text = "$0,00";
            // 
            // lblIngresos
            // 
            this.lblIngresos.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIngresos.Appearance.Options.UseFont = true;
            this.lblIngresos.Location = new System.Drawing.Point(1314, 462);
            this.lblIngresos.MinimumSize = new System.Drawing.Size(0, 25);
            this.lblIngresos.Name = "lblIngresos";
            this.lblIngresos.Size = new System.Drawing.Size(50, 25);
            this.lblIngresos.StyleController = this.layoutControl1;
            this.lblIngresos.TabIndex = 13;
            this.lblIngresos.Text = "$0,00";
            // 
            // lblHoraApertura
            // 
            this.lblHoraApertura.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraApertura.Appearance.Options.UseFont = true;
            this.lblHoraApertura.Location = new System.Drawing.Point(1313, 76);
            this.lblHoraApertura.Name = "lblHoraApertura";
            this.lblHoraApertura.Size = new System.Drawing.Size(51, 25);
            this.lblHoraApertura.StyleController = this.layoutControl1;
            this.lblHoraApertura.TabIndex = 11;
            this.lblHoraApertura.Text = "00:00";
            // 
            // lblHoraCierre
            // 
            this.lblHoraCierre.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoraCierre.Appearance.Options.UseFont = true;
            this.lblHoraCierre.Location = new System.Drawing.Point(1313, 105);
            this.lblHoraCierre.Name = "lblHoraCierre";
            this.lblHoraCierre.Size = new System.Drawing.Size(51, 25);
            this.lblHoraCierre.StyleController = this.layoutControl1;
            this.lblHoraCierre.TabIndex = 9;
            this.lblHoraCierre.Text = "00:00";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(942, 433);
            this.labelControl1.MinimumSize = new System.Drawing.Size(125, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(125, 25);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Monto Inicial:";
            // 
            // gridCajas
            // 
            this.gridCajas.Location = new System.Drawing.Point(24, 66);
            this.gridCajas.MainView = this.gridViewCajas;
            this.gridCajas.Name = "gridCajas";
            this.gridCajas.Size = new System.Drawing.Size(878, 732);
            this.gridCajas.TabIndex = 4;
            this.gridCajas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCajas});
            this.gridCajas.Resize += new System.EventHandler(this.CajaGestion_Resize);
            // 
            // gridViewCajas
            // 
            this.gridViewCajas.GridControl = this.gridCajas;
            this.gridViewCajas.Name = "gridViewCajas";
            this.gridViewCajas.OptionsBehavior.Editable = false;
            this.gridViewCajas.OptionsView.ShowGroupPanel = false;
            this.gridViewCajas.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewCajas_FocusedRowChanged);
            // 
            // botonNuevo
            // 
            this.botonNuevo.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("botonNuevo.Appearance.Image")));
            this.botonNuevo.Appearance.Options.UseImage = true;
            this.botonNuevo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botonNuevo.ImageOptions.Image")));
            this.botonNuevo.Location = new System.Drawing.Point(698, 24);
            this.botonNuevo.MinimumSize = new System.Drawing.Size(0, 40);
            this.botonNuevo.Name = "botonNuevo";
            this.botonNuevo.Size = new System.Drawing.Size(204, 40);
            this.botonNuevo.StyleController = this.layoutControl1;
            this.botonNuevo.TabIndex = 7;
            this.botonNuevo.Text = "Abrir Caja";
            this.botonNuevo.Click += new System.EventHandler(this.botonNuevo_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(942, 76);
            this.labelControl5.MaximumSize = new System.Drawing.Size(125, 0);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(125, 19);
            this.labelControl5.StyleController = this.layoutControl1;
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Hora Apertura:";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(942, 462);
            this.labelControl7.MinimumSize = new System.Drawing.Size(125, 25);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(125, 25);
            this.labelControl7.StyleController = this.layoutControl1;
            this.labelControl7.TabIndex = 8;
            this.labelControl7.Text = "Ingresos:";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(942, 491);
            this.labelControl9.MinimumSize = new System.Drawing.Size(125, 25);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(125, 25);
            this.labelControl9.StyleController = this.layoutControl1;
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "Egresos:";
            // 
            // txtComentario
            // 
            this.txtComentario.Location = new System.Drawing.Point(942, 689);
            this.txtComentario.MaximumSize = new System.Drawing.Size(0, 65);
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(422, 57);
            this.txtComentario.StyleController = this.layoutControl1;
            this.txtComentario.TabIndex = 28;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup5});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1400, 822);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.emptySpaceItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(906, 802);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridCajas;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(882, 736);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.botonNuevo;
            this.layoutControlItem4.Location = new System.Drawing.Point(674, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(208, 42);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(208, 42);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(208, 42);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(674, 42);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.frameCaja,
            this.layoutControlGroup4,
            this.layoutControlGroup3});
            this.layoutControlGroup5.Location = new System.Drawing.Point(906, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(474, 802);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // frameCaja
            // 
            this.frameCaja.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frameCaja.AppearanceGroup.Options.UseFont = true;
            this.frameCaja.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem18,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.frameCaja.Location = new System.Drawing.Point(0, 0);
            this.frameCaja.Name = "frameCaja";
            this.frameCaja.Size = new System.Drawing.Size(450, 122);
            this.frameCaja.Text = "Caja #";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.labelControl5;
            this.layoutControlItem7.CustomizationFormText = "Hora Apertura:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(129, 29);
            this.layoutControlItem7.Text = "Hora Apertura:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.labelControl15;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(129, 29);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lblHoraCierre;
            this.layoutControlItem3.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControlItem3.Location = new System.Drawing.Point(129, 29);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(297, 29);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lblHoraApertura;
            this.layoutControlItem5.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControlItem5.Location = new System.Drawing.Point(129, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(297, 29);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem12,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem13,
            this.layoutControlItem19,
            this.layoutControlItem8,
            this.layoutControlItem15,
            this.layoutControlItem11,
            this.emptySpaceItem2,
            this.layoutControlItem6,
            this.emptySpaceItem4,
            this.layoutControlItem14});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 357);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(450, 421);
            this.layoutControlGroup4.Text = "Detalle";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelControl1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(129, 29);
            this.layoutControlItem2.Text = "Hora Apertura:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.lblMontoInicial;
            this.layoutControlItem12.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControlItem12.Location = new System.Drawing.Point(129, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(297, 29);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.lblIngresos;
            this.layoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControlItem10.Location = new System.Drawing.Point(129, 29);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(297, 29);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelControl7;
            this.layoutControlItem9.CustomizationFormText = "Hora Apertura:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(129, 29);
            this.layoutControlItem9.Text = "Hora Apertura:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtComentario;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 256);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(426, 61);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.botonCerrarCaja;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 317);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(426, 40);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtEfectivoCaja;
            this.layoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControlItem13.Location = new System.Drawing.Point(142, 137);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(284, 36);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.labelControl16;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 227);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(426, 29);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.labelControl6;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(142, 36);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.lblEgresos;
            this.layoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControlItem15.Location = new System.Drawing.Point(129, 58);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(297, 29);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.labelControl9;
            this.layoutControlItem11.CustomizationFormText = "Hora Apertura:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(129, 29);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 112);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(426, 25);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(426, 25);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(426, 25);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.BackColor = System.Drawing.Color.DarkOrange;
            this.layoutControlItem6.AppearanceItemCaption.BorderColor = System.Drawing.Color.Red;
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseBorderColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.lblCierreParcial;
            this.layoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 87);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem6.Size = new System.Drawing.Size(426, 25);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, -1, 0, 0);
            this.layoutControlItem6.Text = "Estimado en Caja";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(145, 23);
            this.layoutControlItem6.TrimClientAreaToControl = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 173);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(426, 25);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(426, 25);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(426, 25);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.BackColor = System.Drawing.Color.DarkOrange;
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem14.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseBorderColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.Control = this.lblTotal;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 198);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(426, 29);
            this.layoutControlItem14.Text = "Total";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(145, 23);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem17});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 122);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(450, 235);
            this.layoutControlGroup3.Text = "Movimientos de Caja";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.botonNuevoMovimiento;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(426, 40);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.gridMovimientosCaja;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(426, 131);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // CajaGestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1400, 822);
            this.Controls.Add(this.layoutControl1);
            this.Name = "CajaGestion";
            this.Text = " Caja Consulta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CajaConsulta_FormClosing);
            this.Shown += new System.EventHandler(this.CajaConsulta_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMovimientosCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMovimientosCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEfectivoCaja.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCajas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCajas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComentario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gridCajas;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCajas;
        private DevExpress.XtraEditors.SimpleButton botonNuevo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup frameCaja;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.LabelControl lblHoraApertura;
        private DevExpress.XtraEditors.LabelControl lblHoraCierre;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.LabelControl lblIngresos;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.LabelControl lblMontoInicial;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.LabelControl lblEgresos;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit txtEfectivoCaja;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.SimpleButton botonCerrarCaja;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.MemoEdit txtComentario;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LabelControl lblCierreParcial;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.LabelControl lblTotal;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraGrid.GridControl gridMovimientosCaja;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMovimientosCaja;
        private DevExpress.XtraEditors.SimpleButton botonNuevoMovimiento;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
    }
}