﻿using DevExpress.Utils;
using DevExpress.XtraEditors;
using MediatR;
using SimpleManager.Aplicacion.Servicios.Cajas.Comandos.CloseCaja;
using SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaDetalle;
using SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaGestion;
using SimpleManager.SimpleKiosk.Formularios.MovimientoCaja;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleManager.SimpleKiosk.Formularios.Caja
{
    public partial class CajaGestion : XtraForm
    {
        FormWindowState LastWindowState;
        private IMediator Mediador;
        public CajaGestion(IMediator mediador)
        {
            InitializeComponent();
            Mediador = mediador;
        }

        //** Presentacion **//

        private async Task PresentarFormulario()
        {
            Debug.WriteLine("PresentarFormulario");
            await PresentarGrilla();
        }

        private async Task PresentarGrilla()
        {
            var DatosConsulta = await GetDatosConsulta();

            this.gridViewCajas.FocusedRowChanged -= new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewCajas_FocusedRowChanged);

            //guarda posicion
            var topRowIndex = gridViewCajas.TopRowIndex;
            var focusedRowHandle = gridViewCajas.FocusedRowHandle;

            gridCajas.DataSource = DatosConsulta.gridCajas;
            gridViewCajas.Columns["CajaId"].Caption = "Id";
            gridViewCajas.Columns["FechaApertura"].DisplayFormat.FormatType = FormatType.DateTime;
            gridViewCajas.Columns["FechaApertura"].DisplayFormat.FormatString = "dd/MM/yy HH:mm tt";
            gridViewCajas.Columns["FechaCierre"].DisplayFormat.FormatType = FormatType.DateTime;
            gridViewCajas.Columns["FechaCierre"].DisplayFormat.FormatString = "dd/MM/yy HH:mm tt";
            gridViewCajas.Columns["MontoInicial"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewCajas.Columns["MontoInicial"].DisplayFormat.FormatString = "c2";
            gridViewCajas.Columns["EfectivoCaja"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewCajas.Columns["EfectivoCaja"].DisplayFormat.FormatString = "c2";

            gridViewCajas.OptionsBehavior.ReadOnly = true;
            gridViewCajas.OptionsSelection.EnableAppearanceFocusedCell = false;
            gridViewCajas.BestFitColumns();
            //setea posicionamiento
            gridViewCajas.FocusedRowHandle = focusedRowHandle;
            gridViewCajas.TopRowIndex = topRowIndex;

            this.gridViewCajas.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewCajas_FocusedRowChanged);

            var cajaId = CajaIdSeleccionado();

            if (cajaId > 0 )
                await PresentarDetalles(cajaId);
        }

        private async Task PresentarDetalles(int cajaId = 0)
        {
            var DatosConsulta = await GetDatosDetalle(cajaId);

            //PresentarDatosCaja
            frameCaja.Text = "Caja #" + DatosConsulta.CajaId.ToString();
            lblHoraApertura.Text = DatosConsulta.FechaApertura.ToString();
            lblHoraCierre.Text = DatosConsulta.FechaCierre.ToString();

            //PresentarMovimientos
            gridMovimientosCaja.DataSource = DatosConsulta.gridMovimientosCaja;
            gridViewMovimientosCaja.Columns["Fecha"].DisplayFormat.FormatType = FormatType.DateTime;
            gridViewMovimientosCaja.Columns["Fecha"].DisplayFormat.FormatString = "dd/MM/yy HH:mm tt";
            gridViewMovimientosCaja.Columns["Monto"].DisplayFormat.FormatType = FormatType.Numeric;
            gridViewMovimientosCaja.Columns["Monto"].DisplayFormat.FormatString = "c2";
            gridViewMovimientosCaja.OptionsBehavior.ReadOnly = true;
            gridViewMovimientosCaja.BestFitColumns();

            //PresentarDatosDetalle
            lblMontoInicial.Text = DatosConsulta.MontoInicial.ToString("c2");
            lblIngresos.Text = DatosConsulta.Ingresos.ToString("c2");
            lblEgresos.Text = DatosConsulta.Egresos.ToString("c2");
            lblCierreParcial.Text = DatosConsulta.EfectivoEstimado.ToString("c2");
            txtEfectivoCaja.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            txtEfectivoCaja.Properties.Mask.EditMask = "c2";
            txtEfectivoCaja.Properties.Mask.UseMaskAsDisplayFormat = true;
            txtEfectivoCaja.EditValue = DatosConsulta.EfectivoCaja;
            txtComentario.Text = DatosConsulta.Comentario;
            lblTotal.Text = DatosConsulta.Total.ToString("c2");

            //Estados
            txtEfectivoCaja.Enabled = (DatosConsulta.PermiteEditar) ? true : false;
            txtComentario.Enabled = (DatosConsulta.PermiteEditar) ? true : false;
            botonCerrarCaja.Enabled = (DatosConsulta.PermiteEditar) ? true : false;
            botonNuevoMovimiento.Enabled = (DatosConsulta.PermiteEditar) ? true : false;
        }

        private async Task<CajaGestionViewModel> GetDatosConsulta()
        {
            var query = Mediador.Send(new GetCajaGestionQuery());
            await query;

            return query.Result;
        }

        private async Task<CajaDetalleViewModel> GetDatosDetalle(int cajaId)
        {
            var query = Mediador.Send(new GetCajaDetalleQuery { CajaId = cajaId });
            await query;

            return query.Result;
        }

        //** Eventos Botones **//

        private async void botonNuevo_Click(object sender, EventArgs e)
        {
            var frmCajaDatos = new CajaDatos(Mediador);
            frmCajaDatos.StartPosition = FormStartPosition.CenterParent;

            var result = frmCajaDatos.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                await PresentarFormulario();
            }
        }

        private async void botonCerrarCaja_Click(object sender, EventArgs e)
        {
            var id = CajaIdSeleccionado();
            var result = XtraMessageBox.Show("Esta seguro que desea cerrar el Arqueo?", "Confirmacion", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                var message = Mediador.Send(new CloseCajaCommand
                {
                    CajaId = id,
                    EfectivoCaja = (decimal)txtEfectivoCaja.EditValue,
                    Comentario = txtComentario.Text
                });

                await message;
                if (!message.Result.IsValid)
                {
                    XtraMessageBox.Show(message.Result.Errors.FirstOrDefault().ErrorMessage);
                    return;
                }
            }
            await PresentarFormulario();
        }

        private async void botonNuevoMovimiento_Click(object sender, EventArgs e)
        {
            var frmMovimientoCaja = new MovimientoCajaNuevo(Mediador,CajaIdSeleccionado());
            frmMovimientoCaja.StartPosition = FormStartPosition.CenterParent;

            var result = frmMovimientoCaja.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                await PresentarFormulario();
            }
        }

        //** Eventos Controles **//

        private void txtBusqueda_EditValueChanged(object sender, EventArgs e)
        {
            //gridView1.ActiveFilter.Clear();
            //gridView1.ActiveFilter.NonColumnFilter = string.Format("[Descripcion] Like '%{0}%'", txtBusqueda.Text);
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        //** Form Commons **//

        private int CajaIdSeleccionado()
        {

            return (gridViewCajas.GetFocusedRowCellValue("CajaId") != null) ? (int)gridViewCajas.GetFocusedRowCellValue("CajaId") : 0;
        }

        private void CajaConsulta_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }

        private async void CajaConsulta_Shown(object sender, EventArgs e)
        {
            await PresentarFormulario();
        }

        private async void CajaGestion_Resize(object sender, EventArgs e)
        {
            if (WindowState != LastWindowState)
            {
                LastWindowState = WindowState;

                if (WindowState == FormWindowState.Maximized)
                {
                   await PresentarFormulario();
                }
            }
        }

        private async void gridViewCajas_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            Debug.WriteLine("focused row");
            await PresentarDetalles(CajaIdSeleccionado());
        }

        
    }
}
