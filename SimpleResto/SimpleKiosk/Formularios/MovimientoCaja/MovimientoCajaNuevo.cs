﻿using DevExpress.XtraEditors;
using MediatR;
using SimpleManager.Aplicacion.Servicios.MovimientosCaja.Comandos.CreateMovimientoCaja;
using SimpleManager.Dominio.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleManager.SimpleKiosk.Formularios.MovimientoCaja
{
    public partial class MovimientoCajaNuevo : XtraForm
    {
        private int CajaId;
        private IMediator Mediator;

        public MovimientoCajaNuevo(IMediator mediator,int CajaId)
        {
            InitializeComponent();
            Mediator = mediator;

            this.CajaId = CajaId;
        }

        private void PresentarDatos()
        {
            IDictionary<int, string> elist = new Dictionary<int, string>();
            elist.Add(1, "Retiro");
            elist.Add(2, "Deposito");

            cboTipo.Properties.DataSource = elist;
            cboTipo.Properties.DisplayMember = "Value";
            cboTipo.Properties.ValueMember = "Key";
            cboTipo.EditValue = 1;

            txtMonto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            txtMonto.Properties.Mask.EditMask = "c2";
            txtMonto.Properties.Mask.UseMaskAsDisplayFormat = true;

        }
            
        private void botonCancelar_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private async void botonCrear_Click(object sender, EventArgs e)
        {
            //if (CajaId > 0)
            //{
            //    var message = Mediator.Send(new UpdateCajaCommand
            //    {
            //        CajaId = CajaId,
            //        Fecha = (DateTime)dtFecha.EditValue,
            //        ProveedorId = (int?)searchProveedor.EditValue,
            //        ComprobanteNumero = txtComprobanteNumero.ToString(),
            //        Observacion = txtObservacion.Text
            //    });
            //    await message;
            //}
            //else
            //{

            var message = Mediator.Send(new CreateMovimientoCajaCommand
            {
                CajaId = this.CajaId,
                TipoMovimientoCajaId = (int)cboTipo.EditValue,
                Monto = Convert.ToDecimal(txtMonto.EditValue),
                Comentario = txtComentario.Text
            });
            await message;
            if (message.Result.IsValid)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                XtraMessageBox.Show(message.Result.Errors.FirstOrDefault().ErrorMessage);
                return;
            }
            //}

        }

        private void PedidoDatos_Load(object sender, EventArgs e)
        {
            PresentarDatos();
        }

        private void cboTipo_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}
