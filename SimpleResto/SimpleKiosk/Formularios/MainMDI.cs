﻿using DevExpress.Skins;
using MediatR;
using SimpleManager.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SimpleManager.SimpleKiosk.Formularios
{
    public partial class MainMDI : DevExpress.XtraEditors.XtraForm
    {

        private Caja.CajaGestion formCajaConsulta;

        public IMediator Mediator;

        public MainMDI(IMediator mediator)
        {
            InitializeComponent();
            Mediator = mediator;
        }

        private void arqueosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (formCajaConsulta == null)
            {
                formCajaConsulta = new Caja.CajaGestion(Mediator);
                formCajaConsulta.MdiParent = this;
                formCajaConsulta.WindowState = FormWindowState.Maximized;
                formCajaConsulta.FormClosed += (o, ea) => formCajaConsulta = null;
            }
            else
            {

                formCajaConsulta.WindowState = FormWindowState.Maximized;
            }

            formCajaConsulta.Show();
        }
    }
}
