﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;


namespace SimpleKiosk.Formularios
{
    public partial class Unidades_ABM : Form
    {
        public Unidades_ABM()
        {
            InitializeComponent();


        }

        public Unidades_ABM(int id)
        {
            InitializeComponent();
            using (var db = new KioskDBContext())
            {

                var _unidades = db.unidad.ToList();
                gridControl1.DataSource = _unidades;


            }
        }
    }
}
