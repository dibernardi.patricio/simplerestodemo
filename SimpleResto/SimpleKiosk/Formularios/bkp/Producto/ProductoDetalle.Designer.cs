﻿namespace SimpleManager.SimpleKiosk.Formularios
{
    partial class Productos_ABM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.codigoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.descripcionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.stock_actualTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.stock_minimoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.unidad_idSearchLookUpEdit = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.rubro_idSearchLookUpEdit = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.esActivoCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.stock_controlaCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.vencimiento_controlaCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.vencimiento_fechaDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.vencimiento_dias_avisoSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForesActivo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForcodigo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordescripcion = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstock_controla = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstock_actual = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstock_minimo = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForvencimiento_controla = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForvencimiento_fecha = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForvencimiento_dias_aviso = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForunidad_id = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForrubro_id = new DevExpress.XtraLayout.LayoutControlItem();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.codigoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descripcionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_actualTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_minimoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unidad_idSearchLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rubro_idSearchLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.esActivoCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_controlaCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_controlaCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_fechaDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_fechaDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_dias_avisoSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForesActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForcodigo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstock_controla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstock_actual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstock_minimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForvencimiento_controla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForvencimiento_fecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForvencimiento_dias_aviso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForunidad_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForrubro_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.pictureEdit1);
            this.dataLayoutControl1.Controls.Add(this.codigoTextEdit);
            this.dataLayoutControl1.Controls.Add(this.descripcionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.stock_actualTextEdit);
            this.dataLayoutControl1.Controls.Add(this.stock_minimoTextEdit);
            this.dataLayoutControl1.Controls.Add(this.unidad_idSearchLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.rubro_idSearchLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.esActivoCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.stock_controlaCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.vencimiento_controlaCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.vencimiento_fechaDateEdit);
            this.dataLayoutControl1.Controls.Add(this.vencimiento_dias_avisoSpinEdit);
            this.dataLayoutControl1.DataSource = typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(681, 283, 650, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(398, 524);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // codigoTextEdit
            // 
            this.codigoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "codigo", true));
            this.codigoTextEdit.Location = new System.Drawing.Point(136, 24);
            this.codigoTextEdit.Name = "codigoTextEdit";
            this.codigoTextEdit.Size = new System.Drawing.Size(50, 20);
            this.codigoTextEdit.StyleController = this.dataLayoutControl1;
            this.codigoTextEdit.TabIndex = 4;
            // 
            // descripcionTextEdit
            // 
            this.descripcionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "descripcion", true));
            this.descripcionTextEdit.Location = new System.Drawing.Point(136, 48);
            this.descripcionTextEdit.Name = "descripcionTextEdit";
            this.descripcionTextEdit.Size = new System.Drawing.Size(238, 20);
            this.descripcionTextEdit.StyleController = this.dataLayoutControl1;
            this.descripcionTextEdit.TabIndex = 5;
            // 
            // stock_actualTextEdit
            // 
            this.stock_actualTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "stock_actual", true));
            this.stock_actualTextEdit.Location = new System.Drawing.Point(136, 342);
            this.stock_actualTextEdit.Name = "stock_actualTextEdit";
            this.stock_actualTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.stock_actualTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.stock_actualTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.stock_actualTextEdit.Properties.Mask.EditMask = "N0";
            this.stock_actualTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.stock_actualTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.stock_actualTextEdit.Size = new System.Drawing.Size(66, 20);
            this.stock_actualTextEdit.StyleController = this.dataLayoutControl1;
            this.stock_actualTextEdit.TabIndex = 8;
            // 
            // stock_minimoTextEdit
            // 
            this.stock_minimoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "stock_minimo", true));
            this.stock_minimoTextEdit.Location = new System.Drawing.Point(136, 366);
            this.stock_minimoTextEdit.Name = "stock_minimoTextEdit";
            this.stock_minimoTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.stock_minimoTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.stock_minimoTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.stock_minimoTextEdit.Properties.Mask.EditMask = "N0";
            this.stock_minimoTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.stock_minimoTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.stock_minimoTextEdit.Size = new System.Drawing.Size(66, 20);
            this.stock_minimoTextEdit.StyleController = this.dataLayoutControl1;
            this.stock_minimoTextEdit.TabIndex = 9;
            // 
            // unidad_idSearchLookUpEdit
            // 
            this.unidad_idSearchLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "unidad_id", true));
            this.unidad_idSearchLookUpEdit.Location = new System.Drawing.Point(136, 115);
            this.unidad_idSearchLookUpEdit.Name = "unidad_idSearchLookUpEdit";
            this.unidad_idSearchLookUpEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.unidad_idSearchLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.unidad_idSearchLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.unidad_idSearchLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.unidad_idSearchLookUpEdit.Properties.NullText = "";
            this.unidad_idSearchLookUpEdit.Properties.PopupView = this.searchLookUpEdit1View;
            this.unidad_idSearchLookUpEdit.Size = new System.Drawing.Size(90, 20);
            this.unidad_idSearchLookUpEdit.StyleController = this.dataLayoutControl1;
            this.unidad_idSearchLookUpEdit.TabIndex = 13;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // rubro_idSearchLookUpEdit
            // 
            this.rubro_idSearchLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "rubro_id", true));
            this.rubro_idSearchLookUpEdit.Location = new System.Drawing.Point(136, 139);
            this.rubro_idSearchLookUpEdit.Name = "rubro_idSearchLookUpEdit";
            this.rubro_idSearchLookUpEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.rubro_idSearchLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.rubro_idSearchLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.rubro_idSearchLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rubro_idSearchLookUpEdit.Properties.NullText = "";
            this.rubro_idSearchLookUpEdit.Properties.PopupView = this.gridView1;
            this.rubro_idSearchLookUpEdit.Size = new System.Drawing.Size(90, 20);
            this.rubro_idSearchLookUpEdit.StyleController = this.dataLayoutControl1;
            this.rubro_idSearchLookUpEdit.TabIndex = 14;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // esActivoCheckEdit
            // 
            this.esActivoCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "esActivo", true));
            this.esActivoCheckEdit.Location = new System.Drawing.Point(326, 24);
            this.esActivoCheckEdit.Name = "esActivoCheckEdit";
            this.esActivoCheckEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.esActivoCheckEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.esActivoCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.esActivoCheckEdit.Properties.Caption = "";
            this.esActivoCheckEdit.Size = new System.Drawing.Size(19, 19);
            this.esActivoCheckEdit.StyleController = this.dataLayoutControl1;
            this.esActivoCheckEdit.TabIndex = 16;
            // 
            // stock_controlaCheckEdit
            // 
            this.stock_controlaCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "stock_controla", true));
            this.stock_controlaCheckEdit.Location = new System.Drawing.Point(136, 319);
            this.stock_controlaCheckEdit.Name = "stock_controlaCheckEdit";
            this.stock_controlaCheckEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.stock_controlaCheckEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.stock_controlaCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.stock_controlaCheckEdit.Properties.Caption = "";
            this.stock_controlaCheckEdit.Size = new System.Drawing.Size(66, 19);
            this.stock_controlaCheckEdit.StyleController = this.dataLayoutControl1;
            this.stock_controlaCheckEdit.TabIndex = 17;
            // 
            // vencimiento_controlaCheckEdit
            // 
            this.vencimiento_controlaCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "vencimiento_controla", true));
            this.vencimiento_controlaCheckEdit.Location = new System.Drawing.Point(136, 433);
            this.vencimiento_controlaCheckEdit.Name = "vencimiento_controlaCheckEdit";
            this.vencimiento_controlaCheckEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.vencimiento_controlaCheckEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.vencimiento_controlaCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.vencimiento_controlaCheckEdit.Properties.Caption = "";
            this.vencimiento_controlaCheckEdit.Size = new System.Drawing.Size(67, 19);
            this.vencimiento_controlaCheckEdit.StyleController = this.dataLayoutControl1;
            this.vencimiento_controlaCheckEdit.TabIndex = 18;
            // 
            // vencimiento_fechaDateEdit
            // 
            this.vencimiento_fechaDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "vencimiento_fecha", true));
            this.vencimiento_fechaDateEdit.EditValue = null;
            this.vencimiento_fechaDateEdit.Location = new System.Drawing.Point(136, 456);
            this.vencimiento_fechaDateEdit.Name = "vencimiento_fechaDateEdit";
            this.vencimiento_fechaDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vencimiento_fechaDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vencimiento_fechaDateEdit.Properties.DisplayFormat.FormatString = "";
            this.vencimiento_fechaDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.vencimiento_fechaDateEdit.Properties.EditFormat.FormatString = "";
            this.vencimiento_fechaDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.vencimiento_fechaDateEdit.Properties.Mask.EditMask = "";
            this.vencimiento_fechaDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.vencimiento_fechaDateEdit.Size = new System.Drawing.Size(67, 20);
            this.vencimiento_fechaDateEdit.StyleController = this.dataLayoutControl1;
            this.vencimiento_fechaDateEdit.TabIndex = 19;
            // 
            // vencimiento_dias_avisoSpinEdit
            // 
            this.vencimiento_dias_avisoSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleManager.Aplicacion.Servicios.Producto.ProductoModel), "vencimiento_dias_aviso", true));
            this.vencimiento_dias_avisoSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.vencimiento_dias_avisoSpinEdit.Location = new System.Drawing.Point(136, 480);
            this.vencimiento_dias_avisoSpinEdit.Name = "vencimiento_dias_avisoSpinEdit";
            this.vencimiento_dias_avisoSpinEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.vencimiento_dias_avisoSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.vencimiento_dias_avisoSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.vencimiento_dias_avisoSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vencimiento_dias_avisoSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.vencimiento_dias_avisoSpinEdit.Properties.Mask.EditMask = "N0";
            this.vencimiento_dias_avisoSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.vencimiento_dias_avisoSpinEdit.Size = new System.Drawing.Size(67, 20);
            this.vencimiento_dias_avisoSpinEdit.StyleController = this.dataLayoutControl1;
            this.vencimiento_dias_avisoSpinEdit.TabIndex = 20;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(398, 524);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(378, 504);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForesActivo,
            this.ItemForcodigo,
            this.ItemFordescripcion,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(378, 72);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // ItemForesActivo
            // 
            this.ItemForesActivo.Control = this.esActivoCheckEdit;
            this.ItemForesActivo.Location = new System.Drawing.Point(190, 0);
            this.ItemForesActivo.Name = "ItemForesActivo";
            this.ItemForesActivo.Size = new System.Drawing.Size(135, 24);
            this.ItemForesActivo.Text = "es Activo";
            this.ItemForesActivo.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForcodigo
            // 
            this.ItemForcodigo.Control = this.codigoTextEdit;
            this.ItemForcodigo.Location = new System.Drawing.Point(0, 0);
            this.ItemForcodigo.Name = "ItemForcodigo";
            this.ItemForcodigo.Size = new System.Drawing.Size(166, 24);
            this.ItemForcodigo.Text = "Codigo";
            this.ItemForcodigo.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemFordescripcion
            // 
            this.ItemFordescripcion.Control = this.descripcionTextEdit;
            this.ItemFordescripcion.Location = new System.Drawing.Point(0, 24);
            this.ItemFordescripcion.Name = "ItemFordescripcion";
            this.ItemFordescripcion.Size = new System.Drawing.Size(354, 24);
            this.ItemFordescripcion.Text = "Descripcion";
            this.ItemFordescripcion.TextSize = new System.Drawing.Size(109, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(325, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(29, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(166, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(24, 24);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstock_controla,
            this.ItemForstock_actual,
            this.ItemForstock_minimo,
            this.emptySpaceItem4});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 276);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(378, 114);
            this.layoutControlGroup4.Text = "Stock";
            // 
            // ItemForstock_controla
            // 
            this.ItemForstock_controla.Control = this.stock_controlaCheckEdit;
            this.ItemForstock_controla.Location = new System.Drawing.Point(0, 0);
            this.ItemForstock_controla.Name = "ItemForstock_controla";
            this.ItemForstock_controla.Size = new System.Drawing.Size(182, 23);
            this.ItemForstock_controla.Text = "Controla Stock";
            this.ItemForstock_controla.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForstock_actual
            // 
            this.ItemForstock_actual.Control = this.stock_actualTextEdit;
            this.ItemForstock_actual.Location = new System.Drawing.Point(0, 23);
            this.ItemForstock_actual.Name = "ItemForstock_actual";
            this.ItemForstock_actual.Size = new System.Drawing.Size(182, 24);
            this.ItemForstock_actual.Text = "Stock Actual";
            this.ItemForstock_actual.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForstock_minimo
            // 
            this.ItemForstock_minimo.Control = this.stock_minimoTextEdit;
            this.ItemForstock_minimo.Location = new System.Drawing.Point(0, 47);
            this.ItemForstock_minimo.Name = "ItemForstock_minimo";
            this.ItemForstock_minimo.Size = new System.Drawing.Size(182, 24);
            this.ItemForstock_minimo.Text = "Stock Minimo";
            this.ItemForstock_minimo.TextSize = new System.Drawing.Size(109, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(182, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(172, 71);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForvencimiento_controla,
            this.ItemForvencimiento_fecha,
            this.ItemForvencimiento_dias_aviso,
            this.emptySpaceItem5});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 390);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(378, 114);
            this.layoutControlGroup5.Text = "Vencimiento";
            // 
            // ItemForvencimiento_controla
            // 
            this.ItemForvencimiento_controla.Control = this.vencimiento_controlaCheckEdit;
            this.ItemForvencimiento_controla.Location = new System.Drawing.Point(0, 0);
            this.ItemForvencimiento_controla.Name = "ItemForvencimiento_controla";
            this.ItemForvencimiento_controla.Size = new System.Drawing.Size(183, 23);
            this.ItemForvencimiento_controla.Text = "Controla Vencimiento";
            this.ItemForvencimiento_controla.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForvencimiento_fecha
            // 
            this.ItemForvencimiento_fecha.Control = this.vencimiento_fechaDateEdit;
            this.ItemForvencimiento_fecha.Location = new System.Drawing.Point(0, 23);
            this.ItemForvencimiento_fecha.Name = "ItemForvencimiento_fecha";
            this.ItemForvencimiento_fecha.Size = new System.Drawing.Size(183, 24);
            this.ItemForvencimiento_fecha.Text = "Fecha Vencimiento";
            this.ItemForvencimiento_fecha.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForvencimiento_dias_aviso
            // 
            this.ItemForvencimiento_dias_aviso.Control = this.vencimiento_dias_avisoSpinEdit;
            this.ItemForvencimiento_dias_aviso.Location = new System.Drawing.Point(0, 47);
            this.ItemForvencimiento_dias_aviso.Name = "ItemForvencimiento_dias_aviso";
            this.ItemForvencimiento_dias_aviso.Size = new System.Drawing.Size(183, 24);
            this.ItemForvencimiento_dias_aviso.Text = "Dias Aviso Vencimiento";
            this.ItemForvencimiento_dias_aviso.TextSize = new System.Drawing.Size(109, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(183, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(171, 71);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForunidad_id,
            this.ItemForrubro_id,
            this.layoutControlItem1,
            this.emptySpaceItem3});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(378, 204);
            this.layoutControlGroup6.Text = "General";
            // 
            // ItemForunidad_id
            // 
            this.ItemForunidad_id.Control = this.unidad_idSearchLookUpEdit;
            this.ItemForunidad_id.Location = new System.Drawing.Point(0, 0);
            this.ItemForunidad_id.Name = "ItemForunidad_id";
            this.ItemForunidad_id.Size = new System.Drawing.Size(206, 24);
            this.ItemForunidad_id.Text = "Unidad";
            this.ItemForunidad_id.TextSize = new System.Drawing.Size(109, 13);
            // 
            // ItemForrubro_id
            // 
            this.ItemForrubro_id.Control = this.rubro_idSearchLookUpEdit;
            this.ItemForrubro_id.Location = new System.Drawing.Point(0, 24);
            this.ItemForrubro_id.Name = "ItemForrubro_id";
            this.ItemForrubro_id.Size = new System.Drawing.Size(206, 24);
            this.ItemForrubro_id.Text = "Rubro";
            this.ItemForrubro_id.TextSize = new System.Drawing.Size(109, 13);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(230, 115);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Size = new System.Drawing.Size(144, 157);
            this.pictureEdit1.StyleController = this.dataLayoutControl1;
            this.pictureEdit1.TabIndex = 21;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pictureEdit1;
            this.layoutControlItem1.Location = new System.Drawing.Point(206, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(148, 161);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(206, 121);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Productos_ABM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 524);
            this.Controls.Add(this.dataLayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Productos_ABM";
            this.Text = "ABM Productos";
            this.Load += new System.EventHandler(this.Productos_ABM_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.codigoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descripcionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_actualTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_minimoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unidad_idSearchLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rubro_idSearchLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.esActivoCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stock_controlaCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_controlaCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_fechaDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_fechaDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vencimiento_dias_avisoSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForesActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForcodigo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstock_controla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstock_actual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstock_minimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForvencimiento_controla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForvencimiento_fecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForvencimiento_dias_aviso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForunidad_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForrubro_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit codigoTextEdit;
        private DevExpress.XtraEditors.TextEdit descripcionTextEdit;
        private DevExpress.XtraEditors.TextEdit stock_actualTextEdit;
        private DevExpress.XtraEditors.TextEdit stock_minimoTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForcodigo;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordescripcion;
        private DevExpress.XtraLayout.LayoutControlItem ItemForesActivo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstock_controla;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstock_actual;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstock_minimo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForvencimiento_controla;
        private DevExpress.XtraLayout.LayoutControlItem ItemForvencimiento_fecha;
        private DevExpress.XtraLayout.LayoutControlItem ItemForvencimiento_dias_aviso;
        private DevExpress.XtraEditors.SearchLookUpEdit unidad_idSearchLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.SearchLookUpEdit rubro_idSearchLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.CheckEdit esActivoCheckEdit;
        private DevExpress.XtraEditors.CheckEdit stock_controlaCheckEdit;
        private DevExpress.XtraEditors.CheckEdit vencimiento_controlaCheckEdit;
        private DevExpress.XtraEditors.DateEdit vencimiento_fechaDateEdit;
        private DevExpress.XtraEditors.SpinEdit vencimiento_dias_avisoSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForunidad_id;
        private DevExpress.XtraLayout.LayoutControlItem ItemForrubro_id;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}