﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SimpleKiosk.Formularios
{
    public partial class ProductosConsulta : Form
    {
        public Productos_Consulta()
        {
            InitializeComponent();

            using (var db = new KioskDBContext())
            {
                var _productos = db.producto.ToList();
                gridControl1.DataSource = _productos;
            }

        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = true ;
        }
    }
}
