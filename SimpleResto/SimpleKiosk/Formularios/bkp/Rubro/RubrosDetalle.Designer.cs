﻿namespace SimpleKiosk.Formularios
{
    partial class Rubros_ABM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Rubros_ABM));
            this.dataLayoutMainControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.botonCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.botonGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.codigoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.tipo_iva_idLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.esActivoTextEdit = new DevExpress.XtraEditors.CheckEdit();
            this.descripcionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemFortipo_iva_id = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForcodigo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForesActivo = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemFordescripcion = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutMainControl)).BeginInit();
            this.dataLayoutMainControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.codigoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipo_iva_idLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.esActivoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.descripcionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFortipo_iva_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForcodigo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForesActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutMainControl
            // 
            this.dataLayoutMainControl.Controls.Add(this.botonCancelar);
            this.dataLayoutMainControl.Controls.Add(this.botonGuardar);
            this.dataLayoutMainControl.Controls.Add(this.codigoTextEdit);
            this.dataLayoutMainControl.Controls.Add(this.tipo_iva_idLookUpEdit);
            this.dataLayoutMainControl.Controls.Add(this.esActivoTextEdit);
            this.dataLayoutMainControl.Controls.Add(this.descripcionTextEdit);
            this.dataLayoutMainControl.DataSource = typeof(SimpleKiosk.rubro);
            this.dataLayoutMainControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutMainControl.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutMainControl.Name = "dataLayoutMainControl";
            this.dataLayoutMainControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(283, 151, 650, 400);
            this.dataLayoutMainControl.Root = this.layoutControlGroup1;
            this.dataLayoutMainControl.Size = new System.Drawing.Size(515, 161);
            this.dataLayoutMainControl.TabIndex = 0;
            this.dataLayoutMainControl.Text = "dataLayoutControl1";
            // 
            // botonCancelar
            // 
            this.botonCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botonCancelar.ImageOptions.Image")));
            this.botonCancelar.Location = new System.Drawing.Point(364, 110);
            this.botonCancelar.Name = "botonCancelar";
            this.botonCancelar.Size = new System.Drawing.Size(139, 36);
            this.botonCancelar.StyleController = this.dataLayoutMainControl;
            this.botonCancelar.TabIndex = 1;
            this.botonCancelar.Text = "Cancelar";
            this.botonCancelar.Click += new System.EventHandler(this.botonCancelar_Click);
            // 
            // botonGuardar
            // 
            this.botonGuardar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.botonGuardar.Appearance.Options.UseBackColor = true;
            this.botonGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("botonGuardar.ImageOptions.Image")));
            this.botonGuardar.Location = new System.Drawing.Point(215, 110);
            this.botonGuardar.Name = "botonGuardar";
            this.botonGuardar.Size = new System.Drawing.Size(145, 36);
            this.botonGuardar.StyleController = this.dataLayoutMainControl;
            this.botonGuardar.TabIndex = 2;
            this.botonGuardar.Text = "Guardar";
            this.botonGuardar.Click += new System.EventHandler(this.botonGuardar_Click);
            // 
            // codigoTextEdit
            // 
            this.codigoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleKiosk.rubro), "codigo", true));
            this.codigoTextEdit.Location = new System.Drawing.Point(80, 48);
            this.codigoTextEdit.Name = "codigoTextEdit";
            this.codigoTextEdit.Size = new System.Drawing.Size(411, 20);
            this.codigoTextEdit.StyleController = this.dataLayoutMainControl;
            this.codigoTextEdit.TabIndex = 4;
            // 
            // tipo_iva_idLookUpEdit
            // 
            this.tipo_iva_idLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleKiosk.rubro), "tipo_iva_id", true));
            this.tipo_iva_idLookUpEdit.Location = new System.Drawing.Point(80, 72);
            this.tipo_iva_idLookUpEdit.Name = "tipo_iva_idLookUpEdit";
            this.tipo_iva_idLookUpEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.tipo_iva_idLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.tipo_iva_idLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.tipo_iva_idLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tipo_iva_idLookUpEdit.Properties.NullText = "";
            this.tipo_iva_idLookUpEdit.Size = new System.Drawing.Size(115, 20);
            this.tipo_iva_idLookUpEdit.StyleController = this.dataLayoutMainControl;
            this.tipo_iva_idLookUpEdit.TabIndex = 7;
            // 
            // esActivoTextEdit
            // 
            this.esActivoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleKiosk.rubro), "esActivo", true, System.Windows.Forms.DataSourceUpdateMode.Never));
            this.esActivoTextEdit.EditValue = "0";
            this.esActivoTextEdit.EnterMoveNextControl = true;
            this.esActivoTextEdit.Location = new System.Drawing.Point(384, 72);
            this.esActivoTextEdit.Name = "esActivoTextEdit";
            this.esActivoTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.esActivoTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.esActivoTextEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.esActivoTextEdit.Properties.Caption = "es Activo";
            this.esActivoTextEdit.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5;
            this.esActivoTextEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.esActivoTextEdit.Properties.ValueChecked = "1";
            this.esActivoTextEdit.Properties.ValueUnchecked = "0";
            this.esActivoTextEdit.Size = new System.Drawing.Size(107, 22);
            this.esActivoTextEdit.StyleController = this.dataLayoutMainControl;
            this.esActivoTextEdit.TabIndex = 11;
            // 
            // descripcionTextEdit
            // 
            this.descripcionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", typeof(SimpleKiosk.rubro), "descripcion", true));
            this.descripcionTextEdit.Location = new System.Drawing.Point(80, 24);
            this.descripcionTextEdit.Name = "descripcionTextEdit";
            this.descripcionTextEdit.Size = new System.Drawing.Size(411, 20);
            this.descripcionTextEdit.StyleController = this.dataLayoutMainControl;
            this.descripcionTextEdit.TabIndex = 12;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(515, 161);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlGroup3,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(495, 141);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.botonCancelar;
            this.layoutControlItem2.Location = new System.Drawing.Point(352, 98);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(143, 43);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.botonGuardar;
            this.layoutControlItem1.Location = new System.Drawing.Point(203, 98);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(149, 43);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFortipo_iva_id,
            this.ItemForcodigo,
            this.ItemForesActivo,
            this.emptySpaceItem1,
            this.ItemFordescripcion});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(495, 98);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // ItemFortipo_iva_id
            // 
            this.ItemFortipo_iva_id.Control = this.tipo_iva_idLookUpEdit;
            this.ItemFortipo_iva_id.Location = new System.Drawing.Point(0, 48);
            this.ItemFortipo_iva_id.Name = "ItemFortipo_iva_id";
            this.ItemFortipo_iva_id.Size = new System.Drawing.Size(175, 26);
            this.ItemFortipo_iva_id.Text = "Tipo IVA";
            this.ItemFortipo_iva_id.TextSize = new System.Drawing.Size(53, 13);
            // 
            // ItemForcodigo
            // 
            this.ItemForcodigo.Control = this.codigoTextEdit;
            this.ItemForcodigo.Location = new System.Drawing.Point(0, 24);
            this.ItemForcodigo.Name = "ItemForcodigo";
            this.ItemForcodigo.Size = new System.Drawing.Size(471, 24);
            this.ItemForcodigo.Text = "Codigo";
            this.ItemForcodigo.TextSize = new System.Drawing.Size(53, 13);
            // 
            // ItemForesActivo
            // 
            this.ItemForesActivo.Control = this.esActivoTextEdit;
            this.ItemForesActivo.Location = new System.Drawing.Point(360, 48);
            this.ItemForesActivo.Name = "ItemForesActivo";
            this.ItemForesActivo.Size = new System.Drawing.Size(111, 26);
            this.ItemForesActivo.Text = "es Activo";
            this.ItemForesActivo.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForesActivo.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(175, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(185, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemFordescripcion
            // 
            this.ItemFordescripcion.Control = this.descripcionTextEdit;
            this.ItemFordescripcion.Location = new System.Drawing.Point(0, 0);
            this.ItemFordescripcion.Name = "ItemFordescripcion";
            this.ItemFordescripcion.Size = new System.Drawing.Size(471, 24);
            this.ItemFordescripcion.Text = "descripcion";
            this.ItemFordescripcion.TextSize = new System.Drawing.Size(53, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(203, 43);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Rubros_ABM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 161);
            this.Controls.Add(this.dataLayoutMainControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Rubros_ABM";
            this.Text = "RubrosABM";
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutMainControl)).EndInit();
            this.dataLayoutMainControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.codigoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipo_iva_idLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.esActivoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.descripcionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFortipo_iva_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForcodigo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForesActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutMainControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit codigoTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForcodigo;
        private DevExpress.XtraLayout.LayoutControlItem ItemFortipo_iva_id;
        private DevExpress.XtraEditors.SimpleButton botonCancelar;
        private DevExpress.XtraEditors.SimpleButton botonGuardar;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LookUpEdit tipo_iva_idLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForesActivo;
        private DevExpress.XtraEditors.CheckEdit esActivoTextEdit;
        private DevExpress.XtraEditors.TextEdit descripcionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordescripcion;
    }
}