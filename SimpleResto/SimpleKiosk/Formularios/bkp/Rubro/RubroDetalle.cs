﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleKiosk.dbScope;
using SimpleKiosk.Managers;
using SimpleKiosk.Dominio;

namespace SimpleKiosk.Formularios
{
    public partial class Rubros_ABM : Form
    {
        private readonly IAmbientDbContextLocator _ambientDbContextLocator;
        
        private KioskDBContext db
        {
            get
            {
                var dbContext = _ambientDbContextLocator.Get<KioskDBContext>();

                if (dbContext == null)
                    throw new InvalidOperationException("No ambient DbContext of type UserManagementDbContext found. This means that this repository method has been called outside of the scope of a DbContextScope. A repository must only be accessed within the scope of a DbContextScope, which takes care of creating the DbContext instances that the repositories need and making them available as ambient contexts. This is what ensures that, for any given DbContext-derived type, the same instance is used throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory in your top-level business logic service method to create a DbContextScope that wraps the entire business transaction that your service method implements. Then access this repository within that scope. Refer to the comments in the IDbContextScope.cs file for more details.");

                return dbContext;
            }
        }

        //constructor para NUEVO registro
        public Rubros_ABM(IAmbientDbContextLocator ambientDbContextLocator)
        {
            if (ambientDbContextLocator == null) throw new ArgumentNullException("ambientDbContextLocator");
            _ambientDbContextLocator = ambientDbContextLocator;
            InitializeComponent();
        }

        //constructor para EDICION
        public Rubros_ABM(int id, IAmbientDbContextLocator ambientDbContextLocator)
        {
            if (ambientDbContextLocator == null) throw new ArgumentNullException("ambientDbContextLocator");
            _ambientDbContextLocator = ambientDbContextLocator;
            InitializeComponent();

            var rubroService = new RubroService(_ambientDbContextLocator);
            var rubro = rubroService.Traer(id);

            dataLayoutMainControl.DataSource = rubro;

            var tipoIVAService = new TipoIVAService(_ambientDbContextLocator);
            var tipos_iva = tipoIVAService.TraerTodos();
            tipo_iva_idLookUpEdit.Properties.DataSource = tipos_iva;
            tipo_iva_idLookUpEdit.Properties.DisplayMember = "descripcion";
            tipo_iva_idLookUpEdit.Properties.ValueMember = "id";

            tipo_iva_idLookUpEdit.EditValue = rubro.tipo_iva_id;
        }

        private void botonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void botonGuardar_Click(object sender, EventArgs e)
        {
            //carga un dto con los datos del rubro a guardar
            //var rubroService = new RubroService(_ambientDbContextLocator);
            //if (rubroService.Guardar(dtoRubro))
            //{ this.close}
            //else
	        //{
            //  probar que pasa con la excepcion en aplication.
            //  rubroService.?
            //}


            //con el DTO y los ViewModels ya puedo pasar el dbcontext a la capa de aplicacion.
            // el servicio se encarga de validar el objeto (fluent validation)
            // el test unitario prueba el servicio

            this.Close ();
        }
    }
}
