﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using SimpleKiosk.Managers;
using SimpleKiosk.dbScope;

namespace SimpleKiosk.Formularios
{
    public partial class Rubros_Consulta : Form
    {
        private AmbientDbContextLocator _ambientDB { get; set; }
        private RubroService  _rubroService { get; set; }

        public Rubros_Consulta()
        {
            InitializeComponent();
            var dbContextScopeFactory = new DbContextScopeFactory();

            _ambientDB = new AmbientDbContextLocator();
            _rubroService = new RubroService(_ambientDB);
            dbContextScopeFactory.Create();

            gridRubros.DataSource = _rubroService.TraerTodos();
           
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void botonNuevo_Click(object sender, EventArgs e)
        {
            var formRubrosABM = new Rubros_ABM(_ambientDB);
            formRubrosABM.StartPosition = FormStartPosition.CenterParent;
            formRubrosABM.ShowDialog();
            gridViewRubros.RefreshData();
        }

        private void botonEditar_Click(object sender, EventArgs e)
        {
            var id = Convert.ToInt32 (gridViewRubros.GetFocusedRowCellValue("id").ToString());

            var formRubrosABM = new Rubros_ABM(id,_ambientDB);
            formRubrosABM.StartPosition = FormStartPosition.CenterParent;
            formRubrosABM.ShowDialog();
            gridViewRubros.RefreshData();
        }

    }
}
