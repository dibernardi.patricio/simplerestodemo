﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleKiosk.Formularios;
using System.Windows.Forms;


namespace SimpleKiosk.Formularios
{
    public partial class MainMDI : Form
    {
        public MainMDI()
        {
            InitializeComponent();

        }

        private void administracionProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            var frmProductoConsulta= new Productos_Consulta();
            frmProductoConsulta.MdiParent = this;
            frmProductoConsulta.Show();
           
        }

        private void unidadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var _formUnidades = new Unidades_ABM(1);
            _formUnidades.MdiParent = this;
            _formUnidades.Show();

        }

        private void rubrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var _formRubros = new Rubros_Consulta();
            _formRubros.MdiParent = this;
            _formRubros.Show ();

        }

    }
}
