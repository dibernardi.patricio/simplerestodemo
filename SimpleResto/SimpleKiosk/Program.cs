﻿using DevExpress.Skins;
using SimpleManager.SimpleKiosk.Formularios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SimpleInjector;
using MediatR;
using System.Reflection;
using MediatR.Pipeline;
using System.Threading;
using System.Globalization;
using SimpleManager.Aplicacion.Servicios.Cajas.Consultas.GetCajaGestion;

namespace SimpleManager.SimpleKiosk
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SkinManager.EnableMdiFormSkins();

            var culture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            culture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentCulture = culture;

            string executable = Assembly.GetExecutingAssembly().Location;
            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);

            var mediator = CreateMediator();
            try
            {
                Aplicacion.Inicializador.InicializarBasedeDatos();
                Aplicacion.Inicializador.InicializarMapeos();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
            //DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont = new Font("Tahoma", 11,FontStyle.Regular);

            //var asd = Devices.Printer.PrinterCommon.GetAllPrinters();
            //var a = new Aplicacion.Servicios.Ticket.TicketHandler();
            //a.TicketTest(asd[4].ToString());



            Application.Run(new MainMDI(mediator));
        }

        static IMediator CreateMediator ()
        {
            var container = new Container();
            var assemblies = GetAssemblies().ToArray();
            container.RegisterSingleton<IMediator, Mediator>();
            container.Register(typeof(IRequestHandler<,>), assemblies);

            // we have to do this because by default, generic type definitions (such as the Constrained Notification Handler) won't be registered
            var notificationHandlerTypes = container.GetTypesToRegister(typeof(INotificationHandler<>), assemblies, new TypesToRegisterOptions
            {
                IncludeGenericTypeDefinitions = true,
                IncludeComposites = false,
            });
            container.Collection.Register(typeof(INotificationHandler<>), notificationHandlerTypes);

            //container.Register(() => (TextWriter)writer, Lifestyle.Singleton);


            //Pipeline
            container.Collection.Register(typeof(IPipelineBehavior<,>), Enumerable.Empty<Type>());
            container.Collection.Register(typeof(IRequestPreProcessor<>), Enumerable.Empty<Type>());
            container.Collection.Register(typeof(IRequestPostProcessor<,>), Enumerable.Empty<Type>());
            //container.Collection.Register(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            container.Register(() => new ServiceFactory(container.GetInstance), Lifestyle.Singleton);

            container.Verify();

            return container.GetInstance<IMediator>();
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(IMediator).GetTypeInfo().Assembly;
            yield return typeof(GetCajaGestion).GetTypeInfo().Assembly;
        }



    }
}
